//
//  MWFakeFile.h
//  TEST2
//
//  Created by matthew on Thu Jun 05 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MWFile.h"

@interface MWFakeFile : MWFile {
    NSString *myString;
}
-(id) initWithString: (NSString *) what;

@end
