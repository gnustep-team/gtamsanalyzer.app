//
//  tableBrowser.h
//  TamsAnalyzer
//
//  Created by matthew on Sat Jun 15 2002.
//  Copyright (c) 2002 Matthew Weinstein. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "TAMSCharEngine.h"

@interface tableBrowser : NSDocument {
    IBOutlet NSTableView *theTable;
    NSMutableArray *theAData;
    NSMutableDictionary *theDData;
    NSMutableString *dataType;
    NSMutableArray *hotCodeList;
    IBOutlet NSWindow *myWindow;
    TAMSCharEngine *tce;
 
}
-(NSWindow *) window;
-(void) setAData: (NSMutableArray *) ad;
-(void) setDData: (NSMutableDictionary *) dd;
-(void) setHotCodeList: (NSMutableArray *) hc;
-(void) setDataType: (NSString *) dt;
-(void) clearWindow;
-(void) setupTable;
-(void) buildCrossReference;
-(void)buildCount;
-(void)setCharEngine: (TAMSCharEngine *) who;
- (void)windowControllerDidLoadNib:(NSWindowController *)windowController;
@end
