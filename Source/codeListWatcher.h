/* codeListWatcher */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "MyDocument.h"

@interface codeListWatcher : NSObject
{
    IBOutlet id myCodeList;
    IBOutlet MyDocument *myOwner;
}
- (int) numberOfRowsInTableView: (NSTableView*) tableView;
- (id) tableView: (NSTableView *) tableView objectValueForTableColumn: (NSTableColumn *) tableColumn row:(int) row;
@end
