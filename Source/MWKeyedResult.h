//
//  MWKeyedResult.h
//  avtams
//
//  Created by matthew on Sat Jun 05 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "myResults.h"

@interface myResults (MWKeyedResult)
- (NSData *) keyedDataRepresentation;
-(void) loadKeyedData;



@end
