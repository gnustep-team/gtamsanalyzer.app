//
//  TAMSCharEngine.h
//  TamsAnalyzer
//
//  Created by matthew on Sun Nov 17 2002.
//  Copyright (c) 2002 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ctQChar.h"
#import "coderec.h"
#import "tams.h"
#import "utils.h"
#import "MyDocument.h"
#import "ctLimitCrit.h"
#import "MWFile.h"
#define STARTLIM 1
#define ENDLIM  2
#define INLIM  3
#define OUTLIM 4
#define SCANCODE 1



@interface TAMSCharEngine : NSObject {
    NSString *gTheData;
    MWFile *hotSearchDocument;
    NSMutableArray *gFileSearchList;
    NSEnumerator *searchEnum;
    unsigned  dsLen;
    long newlinecnt;
    id gCurrentDataSource;
    id deadDataSource;
    BOOL eofFlag;
    //for unlimited
     NSMutableArray *ctRunVault;

    //for ctlimitfunc
    NSMutableArray *ctZoneVault;
    //for section runs
    NSMutableArray *tagVault;
    int inLimFlag ;
    int singleLimFlag;
    int escFlag;
    NSMutableString *ctLimString;
    NSMutableArray *ctLimitVault;
    NSScanner *currScan;
    int scanMode;
    BOOL scanEndFlag;
    BOOL scanVeryEndFlag;
    id gCurrentTAMS;
    int noParFlag, allowTabFlag;
    //for line numbers
    unsigned cln;
    NSMutableString *clnBuff;
    BOOL clnMode;
    BOOL hasLineNumbers;

}
-(id) initWithArrayAndStart: (id) who;
-(id) initWithString: (id) who;

#ifdef SCANCODE
-(int) scanNext: (ctQChar *) qc;
#endif

-(id) initWithFile: (id) who;
-(id) initWithFileAndStart: (id) who;
-(id) initWithStringAndStart: (id) who;
-(int) readQChar: (ctQChar *) qc atIndex: (int) where;
-(MWCHAR) readCharAtIndex: (unsigned) where;
-(int) readnext: (ctQChar *) qc;
-(void) resetDataStream;
-(NSArray *)hotFileList;
-(BOOL) getEOF;
-(BOOL) hasLineNumbers;
#ifdef SECTIONSEARCH
-(coderec *) ctOpenSectionRun;
-(void) ctCloseSectionRun;

-(void) ctCloseSectionTagForCode: (NSString *) mycode Coder: (NSString *) mycoder;
-(BOOL) hasSectionRun;
-(int) checkAnd:  (NSArray *) andGrp forSection: (NSMutableArray *) sCodes;
-(void) filterSectionRuns;
-(int) isCurrSectionCode:(ctLimitCrit *)elem forSectionArray: (NSMutableArray *) who;
-(void) ctAddSectionTag: (NSMutableDictionary *) md;
-(coderec *) hotSectionRun;

#endif

-(void) addArrayToSearchList: (NSArray *) who;

-(void) initSearchListSystem;
-(void) addFileToSearchList: (MWFile *)who;
-(void) clearSearchList;
-(void) startSearch;
-(void) resetDataStream;
-(NSString *) gTheData;
-(void) setGTheData: (NSString *) dd;
-(MWFile *) hotSearchDocument;
-(void) setCharLocation: (int) loc;
-(int) getCharLocation;
-(void) setGCurrentDataSource: (id) theSource;
-(id) gCurrentDataSource;
-(void) setNoPar:(int) value;
-(void) setAllowTab: (int) value;
-(void) setEscFlag: (BOOL) value;

/* for unlimited */
-(void) ctInitRV;
-(void) ctAddOpenChar:(ctQChar *) qq;
-(void) ctOpenRun:(ctQChar *) qq;
-(void) ctCloseRun: (ctQChar *) qq;
-(void) ctCloseAllOpenRuns;
-(int) israw;
-(int) isrepeat: (ctQChar *) qq;
-(NSMutableArray*) ctRunVault;

/* from limitfunc */
-(void) setGCurrentTAMS: (id) who;
-(id) gCurrentTAMS;
-(NSMutableArray *) ctZoneVault;
-(int) isStringTrue: (ctLimitCrit *)elem withCode: (NSString *)code withCoder: (NSString *)coder;
-(void) zeroRuns;
-(void) checkZones;
-(void) handlelimit:(NSString *)buff;
-(int) isinlimit
;
-(void) ctAddZone: (ctQChar *)who
;
-(void) ctAddLimChar: (ctQChar *) qq
;
-(void) ctOpenLimRun: (ctQChar *) qq
;
-(void) ctCloseLimRun: (ctQChar *) qq
;
-(void) ctDelZone: (ctQChar *)who;
-(int) handleLimChar: (ctQChar *) qq;
-(void) initLimit;
-(int) isCurrTrue: (ctLimitCrit *)elem;
-(int) isTagTrue: (ctQChar *)s;
-(void) handlelimit: (NSString *)buff;
-(void) ctDelAllZones;
-(int) scanNext: (ctQChar *) qc withWarnings: (BOOL) warn;

@end
