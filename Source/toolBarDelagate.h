/* toolBarDelagate */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface toolBarDelagate : NSObject
{
    IBOutlet id theDocument;
    IBOutlet id mainWindow;
    int buttonNdx;
    NSMutableArray *myButtons;
    NSMutableArray *allButtons;
    NSMutableArray *textPool, *codePool;
    NSToolbar *myToolBar;
}
- (void)setupToolbar;
-(void) addItem: (NSString *) ss;
-(void) addText: (NSString *) ss;
-(void) addBar;

- (NSToolbarItem *)toolbar:(NSToolbar *)toolbar
itemForItemIdentifier:(NSString *)itemIdentifier
willBeInsertedIntoToolbar:(BOOL)flag;
- (NSArray *)toolbarAllowedItemIdentifiers:(NSToolbar*)toolbar;
- (NSArray *)toolbarDefaultItemIdentifiers:(NSToolbar*)toolbar;
-(IBAction) setCode: (id) Sender;
-(BOOL) isButton: (NSString *) who;
-(void) clearButtons;

@end
