//
//  xrefTableColumn.m
//  TamsAnalyzer
//
//  Created by matthew on Sun Jun 16 2002.
//  Copyright (c) 2002 Matthew Weinstein. All rights reserved.
//

#import "xrefTableColumn.h"
#import "xrefTableInfo.h"
#import "tams.h"
#import "utils.h"

@implementation xrefTableColumn
-(id) init
{
    [super init];
    colData = [[NSMutableArray alloc] init];
    return self;
}

-(void) incName: (NSString *) n
{
    FORALL(colData)
    {
	if([[temp theName] isEqualToString: n] == YES)
	{
	    [temp inc];
	}
    
    }
    ENDFORALL;

}

-(int) cntForName: (NSString *) n
{
    FORALL(colData)
    {
	if([[temp theName] isEqualToString: n] == YES)
	{
	    return [temp cnt];
	}
    
    }
    ENDFORALL;
    return 0;

}

-(void) addName: (NSString *) n
{
    xrefTableInfo *tt = [[xrefTableInfo alloc] init];
    [tt setTheName: n];
    [tt setCnt: 1];
    [colData addObject: tt];
}

-(void) addName: (NSString *) n withValue: (int) amt
{
    xrefTableInfo *tt = [[xrefTableInfo alloc] init];
    [tt setTheName: n];
    [tt setCnt: amt];
    [colData addObject: tt];
}

-(BOOL) hasName: (NSString *) n
{
    FORALL(colData)
    {
	if([[temp theName] isEqualToString: n] == YES)
	{
	    return YES;
	}
    
    }
    ENDFORALL;
    return NO;
}

@end

