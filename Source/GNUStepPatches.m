#import "MyDocument.h"
#import "myProject.h"
#import "myResults.h"

@interface MyDocument (GSMyDocument)
-(IBAction) close: (id) sender;
@end

@implementation MyDocument(GSMyDocument)
-(IBAction) close: (id) sender
{
	[self niceClose: sender];
}
@end

@interface myProject (GSMyProject)
-(IBAction) close: (id) sender;
@end

@implementation myProject(GSMyProject)
-(IBAction) close: (id) sender
{
	[self niceClose: sender];
}
@end

@interface myResults (GSMyResults)
-(IBAction) close: (id) sender;
@end

@implementation myResults (GSMyResults)
-(IBAction) close: (id) sender
{
	[self niceClose: sender];
}
@end