/*
 *  MWSummaryReport.h
 *  TEST2
 *
 *  Created by matthew on Sat Oct 18 2003.
 *  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
 *
 */

#import <AppKit/AppKit.h>
#import "myResults.h"
#import "NSMutableNumber.h"

#define SUMMCOUNT 0
#define SUMMSUM 1

#define SRALPHATYPE 0
#define SRINTTYPE 1
#define SRREALTYPE 2
#define SRDATETYPE 3
#define SRCODETYPE 4


@interface MWSummaryReport : NSDocument 
{
    IBOutlet id dataTable;
    IBOutlet NSWindow *myWindow;
    //inputs
    NSMutableArray *data;
    NSMutableArray *groupData;
    int summType, countDup, compLevel, countBlank;
    int summFieldType;
    NSMutableString *summField;
    NSMutableString *summSumField;
    NSMutableString *dateFormat;
    NSMutableArray *sortStack;
    
    //output
    NSMutableArray *summData;
    NSMutableNumber *tot;
}
-(void) buildSummary;
-(void) setData: (NSMutableArray *) d andReport: (NSDictionary *) aReport;
-(void) setDateFormat: (NSString *) df;
-(void) setupTable;
- (id) columnID: (NSString *) col row: (int) rowIndex;

@end

