//
//  ctTagInfo.h
//  TamsAnalyzer
//
//  Created by matthew on Wed May 15 2002.
//  Copyright (c) 2002 Matthew Weinstein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MWFile.h"

@interface ctTagInfo : NSObject {
    NSMutableString *name;
    NSMutableString *coder;
    int location;
    MWFile *doc;
}
-(void) setLocation: (int) l;
-(int) location;
-(id) initWithName: (NSString *) aName;
-(id) initWithName: (NSString *) aName location: (int) l coder: (NSString *) who;
-(MWFile *) document;
-(id) initWithName: (NSString *) aName location: (int) l coder: (NSString *) who doc: (MWFile *) which;
-(NSString *) name;
-(NSString *) coder;
@end
