#import "prefBoss.h"
 NSString *TAPromptKey=@"Do I Prompt";
 NSString *TADateStampKey=@"Do I Time-Date Stamp";
 NSString *TAMacNLKey=@"Do I Mac NL Stamp";
 NSString *TAAutoReloadKey=@"Do I Reload Data Automatically";
 NSString *TACodeFromList=@"Do I use the list or the text field";
 /* to be implemented */
 NSString *TAUseToolTip=@"Do I use the a tooltip in the code column";
 NSString *TACountSections=@"Do I tally empty for every section";
 NSString *TAUseCoder=@"Do I use a coder ID when coding";
  NSString *TACoderID=@"Actual coder identification";
 NSString *TAShowCoder=@"Show codes in raw runs identification";
 NSString *TASavedState=@"Save the state of the workbench";
 NSString *TASavedFileList=@"Saved files for the state";
 NSString *TAEOFIsEnd=@"Should an EOF be a {!END}?";
 NSString *TAZapUnivAtEOF=@"Should the universal list be zapped (removeAll) at EOF";
NSString *TABackendRepeat=@"Should repeat be evaluated at {!end} or at open tag";
NSString *TAScanInit=@"Should an opening file scan the init file as well as itself for !buttons";
 NSString *TANonSimpleRepeat = @"Should isolated repeat values be returned by non-simple searches";
 NSString *TAUnichar = @"Should I export using unichar";
 NSString *TADetachedSheet = @"should sheets float or be detached";
 NSString *TATagColor = @"What color should the tags be";
 NSString *TAZoomFactor = @"How many characters to add on each side for a string search";
 NSString *TAEscapeBraces = @"Recognize \\ as the escape character";
 NSString *TAAutoColor = @"Should the program re-color tags when the file opens";
 NSString *TAScanForLN = @"Should the program be looking for line numbers at each line start";
 NSString *TACommentForCode = @"Should the program present a comment sheet when user hits code button";
NSString *TACancelAfterAddCode = @"should the program unmark everything after add code";
 NSString *TACancelAfterRecode = @"after recoding, should the program remove all the marked";
 NSString *TAGuessUpdate = @"At time of addcode and recode should I update all the records by guessing where the tags appear";
NSString *TASaveGraphviz = @"Should graphviz files be saved via nssavepanel or just opened automatically";
NSString *TAEnableBack = @"Should the data browser enable the back button (YES = lots of memory)";
NSString *TAAVCode = @"The actual code that surronds a time constant";
NSString *TAAVBackspace = @"The amount of backspace in seconds";
NSString *TACodeAV = @"Should AV be coded?";
NSString *TAAutoBackspace = @"Should I backspace when I tell AV to start?";
NSString *TARealColor = @"color saved as an NSColor";
NSString *TAMetaColor = @"sets the color of metas on refresh";
NSString *TABatchOpenFiles = @"should operattions like recode and add code open the files rather than make the changes in the background?";
NSString *TADefaultUnstruct=@"should the program assume that new projects/files are unstructured?";
NSString * TATimeFormatHHMMSS=@"should the program format time as hh:mm:ss?";
NSString *TACheckCodeDefinition=@"should code selections check the definitions as well?";
NSString *TAHCLStackSize =@"Hot code list stack size.";
NSString *TAGUseGV = @"Should TAG use gv as its image viewer?";
NSString *TAGDotDir = @"What is the location of dot?";
NSString *TAGGVDir = @"What is the location of GV?";

prefBoss *gPrefBoss;

@implementation prefBoss

-(id) init
{
   gPrefBoss = self =  [super initWithWindowNibName: @"preference" owner: self];
    return self;


}

-(int) promptValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAPromptKey];

}

-(int) dateTimeValue
{
   return [[NSUserDefaults standardUserDefaults] integerForKey:
	TADateStampKey];

}

-(int) checkCodeDef
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TACheckCodeDefinition];
    
}

-(int) hclStackSize
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAHCLStackSize];
    
}

-(int) macNLValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAMacNLKey];

}
-(int) autoReloadValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAAutoReloadKey];

}
-(int) codeFromListValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TACodeFromList];


}

-(NSColor *) realColor
{
    NSData *dd =  [[NSUserDefaults standardUserDefaults] objectForKey:
	TARealColor];
    return (NSColor *)[NSUnarchiver unarchiveObjectWithData:dd];
}
-(NSColor *) metaColor
{
    NSData *dd =  [[NSUserDefaults standardUserDefaults] objectForKey:
	TAMetaColor];
    return (NSColor *)[NSUnarchiver unarchiveObjectWithData:dd];
    
    
}

-(int) useToolTipValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAUseToolTip];
}


-(int) countSectionsValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TACountSections];
}

-(int) scanInitValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAScanInit];
}
-(int) useCoderValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAUseCoder];
}

-(int) backendRepeatValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TABackendRepeat];
}

-(int) nonSimpleRepeatValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TANonSimpleRepeat];
}
-(int) escapeBracesValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAEscapeBraces];
}
-(int) batchOpenFiles
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TABatchOpenFiles];
}

-(int) unicharValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAUnichar];
} 

-(int) zoomFactorValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAZoomFactor];
}

-(int) saveGraphviz
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TASaveGraphviz];
}

-(int) enableBack
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAEnableBack];
}
-(NSString *) getAVCode
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:
	TAAVCode];
}
-(int) getAVBackspace
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAAVBackspace];
}

-(NSString *) getCoderID
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:
	TACoderID];
}



-(int) useGV
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAGUseGV];
}

-(NSString *) dotDir
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:
	TAGDotDir];
}

-(NSString *) gvDir
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:
	TAGGVDir];
}



-(int) showCoder
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAShowCoder];
}

-(int) EOFIsEndValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAEOFIsEnd];
}

-(int) zapUnivValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAZapUnivAtEOF];
}

-(int) detachedSheetValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TADetachedSheet];
}
-(int) autoColorValue
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAAutoColor];
}

-(int) tagColorValue
{
    return  [[NSUserDefaults standardUserDefaults] integerForKey: TATagColor];
}

-(int) TAScanForLN
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAScanForLN];
}

-(int) commentForCode
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TACommentForCode];
}

-(int) cancelAfterAddCode
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TACancelAfterAddCode];
}
-(int) cancelAfterRecode
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TACancelAfterRecode];
}
-(int) guessUpdate
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAGuessUpdate];
}
-(int) autoBackspace
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TAAutoBackspace];
    
}
-(int) codeAV
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TACodeAV];
    
}

-(int) defaultUnstruct
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TADefaultUnstruct];
    
}
-(int) timeFormatHHMMSS
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:
	TATimeFormatHHMMSS];
    
}
-(void) windowDidLoad
{
    int myTag;
    [dateStamp setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TADateStampKey]];
    [prompt setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAPromptKey]];
     [macNLOption setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAMacNLKey]];
     [checkCodeDef setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
         TACheckCodeDefinition]];
    [autoReloadOption setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAAutoReloadKey]];
    [codeFromListOption setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TACodeFromList]];
    [useToolTip setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAUseToolTip]];
    [countSections setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TACountSections]];
    [useCodeID setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAUseCoder]];
    [codeAV setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TACodeAV]];
    [autoBackspace setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAAutoBackspace]];
    [codeID setStringValue: [[NSUserDefaults standardUserDefaults] stringForKey:
	TACoderID]];
    [AVCode setStringValue: [[NSUserDefaults standardUserDefaults] stringForKey:
	TAAVCode]];
    [AVBackspace setStringValue: [[NSUserDefaults standardUserDefaults] stringForKey:
	TAAVBackspace]];
    [showCoderView setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAShowCoder]];
     [EOFIsEndSwitch setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAEOFIsEnd]];
    [zapUniv setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAZapUnivAtEOF]];
    [escapeBracesOption setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAEscapeBraces]];
    [backendRepeat setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TABackendRepeat]];
    [scanInit setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAScanInit]];
    [zoomFactorView setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAZoomFactor]];
    [scanForLNOption setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAScanForLN]];
    [enableBack setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAEnableBack]];
    [nonSimpleRepeat setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TANonSimpleRepeat]];
    [detachedSheetSwitch setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TADetachedSheet]];
    [commentForCodeOption setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TACommentForCode]];
    [hclStackSize setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAHCLStackSize]];
    [autoColorOption setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAAutoColor]];
    [batchOpenFiles setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TABatchOpenFiles]];
    [cancelAfterAddCode setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TACancelAfterAddCode]];
    [cancelAfterRecode setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TACancelAfterRecode]];
    [saveGraphviz setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TASaveGraphviz]];
    [guessUpdateOption setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAGuessUpdate]];
    [timeFormat setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TATimeFormatHHMMSS]];
    [defaultUnstructSwitch setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TADefaultUnstruct]];
    {
        NSData *dd;
        dd =  [[NSUserDefaults standardUserDefaults] objectForKey:
            TARealColor];
        [realColorWell setColor:(NSColor *)[NSUnarchiver unarchiveObjectWithData: dd]];
    }

    {
        NSData *dd;
        dd =  [[NSUserDefaults standardUserDefaults] objectForKey:
            TAMetaColor];
        [metaColorWell setColor:(NSColor *)[NSUnarchiver unarchiveObjectWithData: dd]];
    }
    
    [charSwitch setAutoenablesItems: YES];
    myTag = [[NSUserDefaults standardUserDefaults] integerForKey:
	TAUnichar];
    [dotDir setStringValue: [[NSUserDefaults standardUserDefaults] stringForKey:
	TAGDotDir]];
    [gvDir setStringValue: [[NSUserDefaults standardUserDefaults] stringForKey:
	TAGGVDir]];
    [useGV setIntValue: [[NSUserDefaults standardUserDefaults] integerForKey:
	TAGUseGV]];
 
    [charSwitch selectItemAtIndex: [charSwitch indexOfItemWithTag: myTag]];
    [tagColorMenu selectItemAtIndex: [[NSUserDefaults standardUserDefaults] integerForKey:
	TATagColor]];
	
}

-(void) windowWillClose:(NSNotification *)aNotification
{
    [[NSUserDefaults standardUserDefaults] setInteger: [zoomFactorView intValue]
	forKey: TAZoomFactor];

}

-(IBAction)tweedleMacNL: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [macNLOption intValue]
	forKey: TAMacNLKey];

}

-(IBAction)tweedleAutoReload: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [autoReloadOption intValue]
	forKey: TAAutoReloadKey];


}
-(IBAction)tweedleAutoColor: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [autoColorOption intValue]
	forKey: TAAutoColor];


}

-(IBAction)tweedleCheckCodeDef: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [checkCodeDef intValue]
                                               forKey: TACheckCodeDefinition];
    
    
}
-(IBAction)tweedleHCLStackSize: (id) sender
{
    int n;
    n = [hclStackSize intValue];
    if(n <= 2) n = 2;
    [[NSUserDefaults standardUserDefaults] setInteger: n
                                               forKey: TAHCLStackSize];
    
    
}
-(IBAction)tweedleSaveGraphviz: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [saveGraphviz intValue]
                                               forKey: TASaveGraphviz];
    
    
}
-(IBAction)tweedleCommentForCode: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [commentForCodeOption intValue]
                                               forKey: TACommentForCode];
    
    
}

-(IBAction)tweedleTimeFormatHHMMSS: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [timeFormat intValue]
	forKey: TATimeFormatHHMMSS];


}
-(IBAction)tweedleDefaultUnstruct: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [defaultUnstructSwitch intValue]
                                               forKey: TADefaultUnstruct];
    
    
}


-(IBAction)tweedleCodeFromList: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [codeFromListOption intValue]
	forKey: TACodeFromList];


}

-(IBAction)tweedleRealColor: (id) sender
{
    NSData *dd = [NSArchiver archivedDataWithRootObject: [realColorWell color]];
    [[NSUserDefaults standardUserDefaults] setObject: dd
                                               forKey: TARealColor];
    if([self autoColorValue])
        [[NSNotificationCenter defaultCenter] postNotificationName: @"rootColorChanged" object: self];
    
    
}

-(IBAction)tweedleMetaColor: (id) sender
{
    NSData *dd = [NSArchiver archivedDataWithRootObject: [metaColorWell color]];
    [[NSUserDefaults standardUserDefaults] setObject: dd
                                              forKey: TAMetaColor];
    if([self autoColorValue])
        [[NSNotificationCenter defaultCenter] postNotificationName: @"rootColorChanged" object: self];
    
    
}


-(IBAction)tweedleDateStamp: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [dateStamp intValue]
	forKey: TADateStampKey];


}
-(IBAction)tweedleScanInit: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [scanInit intValue]
	forKey: TAScanInit];


}
-(IBAction)tweedleZapUniv: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [zapUniv intValue]
	forKey: TAZapUnivAtEOF];


}

-(IBAction)tweedleScanForLN: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [scanForLNOption intValue]
	forKey: TAScanForLN];


}

-(IBAction)tweedleEnableBack: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [enableBack intValue]
                                               forKey: TAEnableBack];
    
    
}


-(IBAction)tweedleEscapeBraces: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [escapeBracesOption intValue]
	forKey: TAEscapeBraces];


}
-(IBAction)tweedleBatchOpenFiles: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [batchOpenFiles intValue]
                                               forKey: TABatchOpenFiles];
    
    
}

-(IBAction)tweedleBackendRepeat: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [backendRepeat intValue]
	forKey: TABackendRepeat];


}
-(IBAction)tweedleNonSimpleRepeat: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [nonSimpleRepeat intValue]
	forKey: TANonSimpleRepeat];


}

-(IBAction)tweedleGuessUpdate: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [guessUpdateOption intValue]
	forKey: TAGuessUpdate];


}


-(IBAction)tweedlePrompt: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [prompt intValue]
	forKey: TAPromptKey];
}
-(IBAction)tweedleUseToolTip: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [useToolTip intValue]
	forKey: TAUseToolTip];
}
-(IBAction)tweedleCountSections: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [countSections intValue]
	forKey: TACountSections];
}

-(IBAction)tweedleUseCoder: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [useCodeID intValue]
	forKey: TAUseCoder];
}
-(IBAction)tweedleShowCoder: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [showCoderView intValue]
	forKey: TAShowCoder];
}

-(IBAction)tweedleDetachedSheet: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [detachedSheetSwitch intValue]
	forKey: TADetachedSheet];
}


- (void)controlTextDidChange:(NSNotification *) aNotification
{
    [[NSUserDefaults standardUserDefaults] setObject: [codeID stringValue]
	forKey: TACoderID];
    [[NSUserDefaults standardUserDefaults] setInteger: [AVBackspace intValue]
                                              forKey: TAAVBackspace];
    [[NSUserDefaults standardUserDefaults] setObject: [AVCode stringValue]
                                              forKey: TAAVCode];

}

-(IBAction)tweedleEOFIsEnd: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [EOFIsEndSwitch intValue]
	forKey: TAEOFIsEnd];
}
-(IBAction)tweedleUnichar: (id) sender
{
    int v;
    v = [[charSwitch selectedItem] tag];
    [[NSUserDefaults standardUserDefaults] setInteger: v
	forKey: TAUnichar];
}
-(IBAction)tweedleTagColor: (id) sender
{
    int v;
    v = [tagColorMenu indexOfSelectedItem];
    [[NSUserDefaults standardUserDefaults] setInteger: v
	forKey: TATagColor];
}

-(IBAction) tweedleCancelAfterRecode: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [cancelAfterRecode intValue]
	forKey: TACancelAfterRecode];
}

-(IBAction) tweedleCancelAfterAddCode: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [cancelAfterAddCode intValue]
	forKey: TACancelAfterAddCode];
}
-(IBAction) tweedleCodeAV: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [codeAV intValue]
                                               forKey: TACodeAV];
}
-(IBAction) tweedleAutoBackspace: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [autoBackspace intValue]
                                               forKey: TAAutoBackspace];
}


-(IBAction)tweedleUseGV: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setInteger: [useGV intValue]
                                               forKey: TAGUseGV];
    
    
}

-(IBAction)tweedleGVDir: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setObject: [gvDir stringValue]
                                               forKey: TAGGVDir];
    
    
}

-(IBAction)tweedleDotDir: (id) sender
{
    [[NSUserDefaults standardUserDefaults] setObject: [dotDir stringValue]
                                               forKey: TAGDotDir];
    
    
}


-(NSArray *) savedState
{
    return [[NSUserDefaults standardUserDefaults] objectForKey: TASavedState];
	
}
-(NSArray *) savedFileList
{
    return [[NSUserDefaults standardUserDefaults] objectForKey: TASavedFileList];
	
}

-(void) saveState: (NSArray *) state withFiles: (NSArray *) fileList
{
     [[NSUserDefaults standardUserDefaults] removeObjectForKey: TASavedState];
     [[NSUserDefaults standardUserDefaults] setObject: state
	forKey: TASavedState];
    if(fileList)
    {
	[[NSUserDefaults standardUserDefaults] setObject: fileList
		forKey: TASavedFileList];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];

}

-(IBAction) niceClose: (id) sender
{
    [panelWindow performClose: sender];
}
@end
