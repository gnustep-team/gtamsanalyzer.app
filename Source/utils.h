//
//  utils.h
//  CocoaTams
//
//  Created by matthew on Mon Apr 15 2002.
//  Copyright (c) 2002 M. Weinstein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#define ADDCHAR(X) appendFormat: @"%C",X
//#define ADDCHAR(X) appendString: charToNSS(X)
#define FORALL(X) {id temp; unsigned  __cnt, __i; __cnt = [X count]; for( __i = 0; __i < __cnt; __i++) {temp = [X objectAtIndex: __i];
#define ENDFORALL }}
#define ISQUOTE(X) ([X characterAtIndex: 0] == '\"' && [X characterAtIndex: ([X length] -1)] == '\"')
#define NSWARNING(X) NSRunAlertPanel(@"Warning!", X, @"Ok", nil, nil);
#define NSMESSAGE(X) NSRunAlertPanel(@"Note:", X, @"Ok", nil, nil);
#define NSYESNOQUESTION(X)  ((NSRunAlertPanel(@"Question:", X, @"No", @"Yes", nil) == 0) ? YES : NO)
#define NSSETHEADER(T,C,S) [[[T tableColumnWithIdentifier: C] headerCell] setStringValue: S]
NSString *charToNSS(char c);
NSMutableString *trimNSS(NSMutableString *s);
NSMutableString *trimCharNSS(NSMutableString *s, unichar ch);
NSMutableArray *convertStringToArray(NSString *theString, char breakChar);
int nsSubString(NSString *first, NSString *sec, BOOL caseInsensitive);
int menuStringComp(id first, id second, void *key);
#define NSALLOC(X,Y)	X *Y = [[X alloc] init]
BOOL addUniqueToArray(NSMutableArray *who, NSString *what);
