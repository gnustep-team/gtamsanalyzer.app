/* codeListSearchWatcher */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "codeListWatcher.h"

@interface codeListSearchWatcher : codeListWatcher
{
    IBOutlet NSTextField *searchString;
    IBOutlet NSTableView *myOwnCodeList;
}
-(IBAction) doRefresh: (id) Sender;
-(BOOL)tableView: (NSTableView *) aTable shouldEditTableColumn: (NSTableColumn *) aCol row: (unsigned) arow;
-(void) displaySelData;
-(IBAction) clearSearch: (id) Sender;
@end
