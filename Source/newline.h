/*
 *  newline.h
 *  xtams
 *
 *  Created by matthew on Wed Dec 19 2001.
 *  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
 *
 */

#define MACNL 0
#define UNIXNL 1
int isnewline(char c);
void setnewline(int flag);
char getnewline(void);
