//
//  MWResultToolbarDelegate.m
//  avtams
//
//  Created by matthew on Tue Mar 09 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import "MWResultToolbarDelegate.h"
#import "tams.h"
#import "tamsutils.h"
#import "utils.h"
#define GSTOOLBAR1
NSString *MWRTBSave=@"Save...";
NSString *MWRTBWorkbench=@"Workbench";

NSString *MWRTBRefresh = @"Refresh";
NSString *MWRTBFindRecord =@"Find record";
NSString *MWRTBSortUpAZ =@"Sort A-Z";
NSString *MWRTBSortUpAZWithin =@"Sort A-Z within";
NSString *MWRTBSortUp09 =@"Sort 0-9";
NSString *MWRTBSortUp09Within =@"Sort 0-9 within";
NSString *MWRTBSortUpCode =@"Sort code";
NSString *MWRTBSortUpCodeWithin =@"Sort code within";

NSString *MWRTBSmartSortUp =@"Sort";
NSString *MWRTBSelectAll =@"Select all";
NSString *MWRTBSelect =@"Select...";
NSString *MWRTBSelectReverse =@"Select reverse";
NSString *MWRTBSelectLess =@"Rmv from sel...";
NSString *MWRTBSelectCodeSet=@"Sel. code set";
NSString *MWRTBSelectionToCodeSet=@"Sel. to code set";
NSString *MWRTBMark =@"Mark";
NSString *MWRTBUnmark =@"Unmark";
NSString *MWRTBAddCode =@"Add code...";
NSString *MWRTBRecode =@"Recode...";
NSString *MWRTBAutoset =@"Autoset...";
NSString *MWRTBDataSummary =@"Data summary...";
NSString *MWRTBExportData =@"Export data...";
NSString *MWRTBCollapseUp=@"Collapse up";
NSString *MWRTBCollapseDown=@"Collapse down";
NSString *MWRTBComment=@"Comment";
NSString *MWRTBColumn=@"Column";
NSString *MWRTBTags=@"Tags?";
NSString *MWRTBAddComment=@"+ Comment";
NSString *MWRTBAddMarkedComment=@"Comment marked";
NSString *MWRTBAddRootCode=@"+Root code";
NSString *MWRTBDelRootCode=@"-Root code";
NSString *MWRTBCodeLevel=@"Code level";
NSString *MWRTBCodeDef=@"Code def.";
NSString *MWRTBSelCodeDef=@"Sel. code def.";
NSString *MWRTBCodeBrowser=@"Code browser";
NSString *MWRTBPlayMedia=@"Play media";

#define MWSTRINGCOMP(X,Y) [X isEqualToString: Y]

@implementation MWResultToolbarDelegate
-(void) dealloc
{
    [myButtons dealloc];
    [allButtons dealloc];
}
-(id) init
{
    [super init];
    myButtons = [[NSMutableArray alloc] init];
#ifdef GSTOOLBAR1
    [myButtons addObject: MWRTBWorkbench];
    [myButtons addObject: MWRTBFindRecord];
    [myButtons addObject: MWRTBRefresh];
    [myButtons addObject: MWRTBTags];
    [myButtons addObject: MWRTBSmartSortUp];
    [myButtons addObject: MWRTBSelect ];
    [myButtons addObject: MWRTBSelectAll];
    [myButtons addObject: MWRTBExportData];
 
#else
    [myButtons addObject: NSToolbarCustomizeToolbarItemIdentifier];
#ifdef COCOAVERSION
    [myButtons addObject: NSToolbarSeparatorItemIdentifier];
#endif
    [myButtons addObject: MWRTBSave];
    [myButtons addObject: MWRTBWorkbench];
#ifdef COCOAVERSION
    [myButtons addObject: NSToolbarSeparatorItemIdentifier];
    
#endif
    [myButtons addObject: MWRTBFindRecord];
    [myButtons addObject: MWRTBRefresh];
    [myButtons addObject: MWRTBSmartSortUp];
    
#ifdef COCOAVERSION
    [myButtons addObject: NSToolbarSeparatorItemIdentifier];    
#endif    
    [myButtons addObject: MWRTBSelectAll];
    [myButtons addObject: MWRTBSelect ];
#ifdef COCOAVERSION
    [myButtons addObject: NSToolbarSeparatorItemIdentifier];    
#endif    

/*
    [myButtons addObject: MWRTBSortUpAZ ];
    [myButtons addObject: MWRTBSortUpAZWithin];
#ifdef COCOAVERSION
    [myButtons addObject: NSToolbarSeparatorItemIdentifier];    
#endif    
    
    [myButtons addObject: MWRTBMark ];
    [myButtons addObject: MWRTBUnmark ];
#ifdef COCOAVERSION
    [myButtons addObject: NSToolbarSeparatorItemIdentifier];    
#endif    
    [myButtons addObject: MWRTBAddCode];
    [myButtons addObject: MWRTBRecode ];
#ifdef COCOAVERSION
    [myButtons addObject: NSToolbarSeparatorItemIdentifier];    
#endif    
    [myButtons addObject: MWRTBAutoset ];
    [myButtons addObject: MWRTBDataSummary];
    
*/    
#endif
    allButtons = [[NSMutableArray alloc] init];
    
    [allButtons addObject: NSToolbarCustomizeToolbarItemIdentifier];
    [allButtons addObject: NSToolbarSpaceItemIdentifier];
    [allButtons addObject: NSToolbarFlexibleSpaceItemIdentifier];
#ifdef COCOAVERSION
    [allButtons addObject: NSToolbarSeparatorItemIdentifier];
#endif
    
    [allButtons addObject: MWRTBSave];
    [allButtons addObject: MWRTBWorkbench];
    [allButtons addObject: MWRTBRefresh];
    [allButtons addObject: MWRTBFindRecord];
    [allButtons addObject: MWRTBSortUpAZ ];
    [allButtons addObject: MWRTBSortUpAZWithin];
    [allButtons addObject: MWRTBSortUp09 ];
    [allButtons addObject: MWRTBSortUp09Within];
    [allButtons addObject: MWRTBSortUpCode ];
    [allButtons addObject: MWRTBSortUpCodeWithin];
    
    [allButtons addObject: MWRTBSmartSortUp];
    [allButtons addObject: MWRTBSelectAll];
    [allButtons addObject: MWRTBSelect ];
    [allButtons addObject: MWRTBSelectReverse ];
    [allButtons addObject: MWRTBSelectLess ];
    [allButtons addObject: MWRTBSelectCodeSet ];
    [allButtons addObject: MWRTBSelectionToCodeSet ];
    
    [allButtons addObject: MWRTBMark ];
    [allButtons addObject: MWRTBUnmark ];
    [allButtons addObject: MWRTBAddCode];
    [allButtons addObject: MWRTBRecode ];
    [allButtons addObject: MWRTBAddRootCode];
    [allButtons addObject: MWRTBDelRootCode];
    [allButtons addObject: MWRTBCodeLevel];

    [allButtons addObject: MWRTBAutoset ];
    [allButtons addObject: MWRTBDataSummary];
    [allButtons addObject: MWRTBExportData];
    [allButtons addObject: MWRTBCollapseUp];
    [allButtons addObject: MWRTBCollapseDown];
    [allButtons addObject: MWRTBComment];
    [allButtons addObject: MWRTBColumn];
    [allButtons addObject: MWRTBTags];
    [allButtons addObject: MWRTBAddComment];
    [allButtons addObject: MWRTBAddMarkedComment];
    [allButtons addObject: MWRTBCodeDef];
    [allButtons addObject: MWRTBSelCodeDef];
    [allButtons addObject: MWRTBCodeBrowser];
    [allButtons addObject: MWRTBPlayMedia];

    return self;
}
- (NSToolbarItem *)toolbar:(NSToolbar *)toolbar
     itemForItemIdentifier:(NSString *)itemIdentifier
 willBeInsertedIntoToolbar:(BOOL)flag
{
    NSToolbarItem *who;
    if(MWSTRINGCOMP(itemIdentifier, MWRTBRefresh))
        who = [self itemWithID: MWRTBRefresh image: @"refresh.tif" action: @selector(refreshResults:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBFindRecord))
            who = [self itemWithID: MWRTBFindRecord image: @"findRecord.tif" action: @selector(fetchRow:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSortUpAZ))
            who = [self itemWithID: MWRTBSortUpAZ image: @"sortAZ.tif" action: @selector(sortUpAZ:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSortUp09))
            who = [self itemWithID: MWRTBSortUp09 image: @"sort09.tif" action: @selector(sortUpInt:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSortUp09Within))
            who = [self itemWithID: MWRTBSortUp09Within image: @"sort09Within.tif" action: @selector(sortUpIntWithin:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSortUpCode))
            who = [self itemWithID: MWRTBSortUpCode image: @"sortCode.tif" action: @selector(sortUpCode:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSortUpCodeWithin))
        who = [self itemWithID: MWRTBSortUpCodeWithin image: @"sortCodeWithin.tif" action: @selector(sortUpCodeWithin:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBAddRootCode))
            who = [self itemWithID: MWRTBAddRootCode image: @"addRoot7.tif" action: @selector(doAddRootCode:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBDelRootCode))
            who = [self itemWithID: MWRTBDelRootCode image: @"delRoot3.tif" action: @selector(doDelRootCode:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSmartSortUp))
            who = [self itemWithID: MWRTBSmartSortUp image: @"smartSort.tif" action: @selector(smartSort:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBCodeLevel))
            who = [self itemWithID: MWRTBCodeLevel image: @"codeLevel.tif" action: @selector(doCodeLevel:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSortUpAZWithin))
            who = [self itemWithID: MWRTBSortUpAZWithin image: @"sortAZWithin.tif" action: @selector(sortUpAZWithin:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSelectAll))
            who = [self itemWithID: MWRTBSelectAll image: @"selectAll3.tif" action: @selector(selectAllRecs:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSelectCodeSet))
            who = [self itemWithID: MWRTBSelectCodeSet image: @"codeset2sel.tif" action: @selector(doSelectCodeSetDialogue:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSelectionToCodeSet))
            who = [self itemWithID: MWRTBSelectionToCodeSet image: @"sel2codeset.tif" action: @selector(turnSelectionIntoCodeSet:)];
     else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBCodeBrowser))
            who = [self itemWithID: MWRTBCodeBrowser image: @"codeBrowser.tif" action: @selector(showCodeBrowser:)];
   
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSelect))
            who = [self itemWithID: MWRTBSelect image: @"selectSome.tif" action: @selector(selectRecs:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSelectReverse))
            who = [self itemWithID: MWRTBSelectReverse image: @"selectReverse.tif" action: @selector(selectReverseRecs:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSelectLess))
            who = [self itemWithID: MWRTBSelectLess image: @"removeFrom.tif" action: @selector(selectFewerRecs:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBMark))
            who = [self itemWithID: MWRTBMark image: @"markRec.tif" action: @selector(doMark:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBUnmark))
            who = [self itemWithID: MWRTBUnmark image: @"unmarkRec2.tif" action: @selector(doUnmark:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBAddComment))
            who = [self itemWithID: MWRTBAddComment image: @"addcomment.tif" action: @selector(doAddComment:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBAddMarkedComment))
            who = [self itemWithID: MWRTBAddMarkedComment image: @"addmarkedcomment.tif" action: @selector(doAppendCommentToMarked:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBAddCode))
            who = [self itemWithID: MWRTBAddCode image: @"addCode.tif" action: @selector(doAddCode:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBRecode))
            who = [self itemWithID: MWRTBRecode image: @"reCode2.tif" action: @selector(doRecode:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBAutoset))
            who = [self itemWithID: MWRTBAutoset image: @"autoSet.tif" action: @selector(createAutoSet:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBDataSummary))
            who = [self itemWithID: MWRTBDataSummary image: @"dataSumm2.tif" action: @selector(runSummaryReport:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSave))
            who = [self itemWithID: MWRTBSave image: @"Save.tif" action: @selector(saveDocument:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBPlayMedia))
            who = [self itemWithID: MWRTBPlayMedia image: @"playMedia.tif" action: @selector(doMediaPlayer:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBWorkbench))
            who = [self itemWithID: MWRTBWorkbench image: @"wbicn.tif" action: @selector(moveWorkBenchForward:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBCollapseUp))
            who = [self itemWithID: MWRTBCollapseUp image: @"collapseUp.tif" action: @selector(doCollapseUp:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBCollapseDown))
            who = [self itemWithID: MWRTBCollapseDown image: @"collapseDown.tif" action: @selector(doCollapseDown:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBComment))
            who = [self itemWithID: MWRTBComment image: @"comment.tif" action: @selector(doShowComment:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBCodeDef))
            who = [self itemWithID: MWRTBCodeDef image: @"def.tif" action: @selector(requestCodeDefinition:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBSelCodeDef))
            who = [self itemWithID: MWRTBSelCodeDef image: @"defFromSel.tif" action: @selector(requestCodeDefinitionOfSelection:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBColumn))
            who = [self itemWithID: MWRTBColumn image: @"column.tif" action: @selector(doBrowseField:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBTags))
            who = [self itemWithID: MWRTBTags image: @"tag2.tif" action: @selector(doSetStripTags:)];
    
    
    else
        if(MWSTRINGCOMP(itemIdentifier, MWRTBExportData))
            who = [self itemWithID: MWRTBExportData image: @"export.tif" action: @selector(exportData:)];
    
    else
        who = [[NSToolbarItem alloc] initWithItemIdentifier: itemIdentifier];
    
    return [who autorelease];  
}

- (NSArray *)toolbarAllowedItemIdentifiers:(NSToolbar*)toolbar
{
    
    return allButtons;
}
- (NSArray *)toolbarDefaultItemIdentifiers:(NSToolbar*)toolbar
{
    //return myButtons;
    return myButtons;
    
}
-(void) clearButtons
{
    [allButtons removeAllObjects];
}
-(BOOL) isButton: (NSString *) who
{
    NSArray *theList;
    theList = [myToolBar items];
    FORALL(theList)
    {
	if([who isEqualToString: [temp itemIdentifier]] == YES)
	    return YES;
    }
    ENDFORALL;
    return NO;
}
- (void)setupToolbar
{
    NSToolbar *toolbar = [[NSToolbar alloc] initWithIdentifier: @"ResultWindowToolbar"];// uniqueString()];
    [toolbar autorelease];
    [toolbar setDelegate:self];
    [toolbar setAllowsUserCustomization:YES];
    [toolbar setAutosavesConfiguration: YES];
    [mainWindow setToolbar: toolbar];
    myToolBar = toolbar;
}
-(NSToolbarItem *) itemWithID: (NSString *) itemID image: (NSString *) fn action: (SEL) act
{
    NSToolbarItem *item = [[NSToolbarItem alloc]
    initWithItemIdentifier:itemID];
        [item setLabel: itemID];
        [item setPaletteLabel:itemID];
        [item setImage:[NSImage imageNamed: fn]];
        [item setTarget:theDocument];
        [item setAction: act];	
        return item;
}
-(void) awakeFromNib
{
    [self setupToolbar];
    
}


@end
