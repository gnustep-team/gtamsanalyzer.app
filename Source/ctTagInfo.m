//
//  ctTagInfo.m
//  TamsAnalyzer
//
//  Created by matthew on Wed May 15 2002.
//  Copyright (c) 2002 Matthew Weinstein. All rights reserved.
//

#import "ctTagInfo.h"


@implementation ctTagInfo
-(void) setLocation: (int) l
{
    location = l;
}
-(int) location {return location;}
-(id) initWithName: (NSString *) aName
{
    self = [super init];
    name = [[NSMutableString alloc] init];
    coder = [[NSMutableString alloc] init];
    [name setString: aName];
    return self;
}
-(NSString *) name{return name;}
-(id) initWithName: (NSString *) aName location: (int) l coder: (NSString *) who
{
    self = [super init];
    name = [[NSMutableString alloc] init];
    [name setString: aName];
    coder = [[NSMutableString alloc] init];
    [coder setString: who];
    location = l;
    doc = nil;
    return self;
}
-(id) initWithName: (NSString *) aName location: (int) l coder: (NSString *) who doc: (MWFile *) which
{
    self = [super init];
    name = [[NSMutableString alloc] init];
    [name setString: aName];
    coder = [[NSMutableString alloc] init];
    [coder setString: who];
    location = l;
    doc = which;
    return self;
}
-(MWFile *) document
{
    return doc;
}
-(NSString *) coder {return coder;}
-(void) dealloc
{
    [name release];
    [coder release];
}
@end
