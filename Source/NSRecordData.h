//
//  NSRecordData.h
//  TamsAnalyzer
//
//  Created by matthew on Mon Apr 29 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSRecordData : NSMutableData {

    unsigned mlength;
}
-(void)addString: (NSString *) what;
-(void)addChar: (char ) ch;
-(void)addTab;
-(void)addNL;
//-(unsigned) length;
@end
