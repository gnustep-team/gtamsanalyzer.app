//
//  NSRecordData.m
//  TamsAnalyzer
//
//  Created by matthew on Mon Apr 29 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import "NSRecordData.h"


@implementation NSRecordData
-(void)addString: (NSString *) what
{    

    mlength+= [what cStringLength];
   // [self setLength: mlength];
    [self appendBytes: [what cString] length: [what cStringLength]];

}
//-(unsigned) mlength {return mlength;}
-(id) init
{
    [super init];
    mlength = 0;
    return self;
}

-(void)addChar: (char ) ch;
{
    char ss[2];
    ss[0] = ch;
    ss[1] = '\0';
    mlength++;
    //[self increaseLengthBy: 1];
    [self appendBytes: ss length: 1];

}
-(void)addTab
{
    [self addChar: '\t'];
}
-(void) addNL
{
    [self addChar: '\n'];
}
@end
