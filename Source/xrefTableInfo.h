//
//  xrefTableInfo.h
//  TamsAnalyzer
//
//  Created by matthew on Sun Jun 16 2002.
//  Copyright (c) 2002 Matthew Weinstein. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface xrefTableInfo : NSObject {
    int cnt;
    NSMutableString *name;
}
-(int) cnt;
-(NSMutableString *) theName;
-(void) setTheName: (NSString *) n;
-(void) inc;
-(void) setCnt: (int) c;

@end
