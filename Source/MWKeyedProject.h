//
//  MGWMyDocumentKeyed.h
//  avtams
//
//  Created by matthew on Sat Jun 05 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//


#import "myProject.h"

@interface myProject(MWKeyedProject)
    - (NSData *) keyedDataRepresentation;
    - (BOOL)loadKeyedData:(NSData *)data;

 
@end
