//
//  xrefTableColumn.h
//  TamsAnalyzer
//
//  Created by matthew on Sun Jun 16 2002.
//  Copyright (c) 2002 Matthew Weinstein. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface xrefTableColumn : NSObject {
    NSMutableArray *colData;
}
-(BOOL) hasName: (NSString *) n;
-(int) cntForName: (NSString *) n;
-(void) incName: (NSString *) n;
-(void) addName: (NSString *) n;
-(void) addName: (NSString *) n withValue: (int) amt;
@end
