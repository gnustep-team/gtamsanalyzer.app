//
//  ctLimitFunc.h
//  CocoaTams
//
//  Created by matthew on Mon Apr 15 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ctQChar.h"
#import "ctLimitCrit.h"
#define STARTLIM 1
#define ENDLIM  2
#define INLIM  3
#define OUTLIM 4

extern NSMutableArray *ctZoneVault;

int isStringTrue(ctLimitCrit *elem, NSString *code, NSString *coder);
void zeroRuns();
void handlelimit(NSString *buff);
int isinlimit()
;
void ctAddZone(ctQChar *who)
;
void ctAddLimChar(ctQChar *qq)
;
void ctOpenLimRun(ctQChar *qq)
;
void ctCloseLimRun(ctQChar *qq)
;
void ctDelZone(ctQChar *who);
int handleLimChar(ctQChar *qq);
void initLimit();
int isCurrTrue(ctLimitCrit *elem);
int isTagTrue(ctQChar *s);
void handlelimit(NSString *buff);
void ctDelAllZones();
