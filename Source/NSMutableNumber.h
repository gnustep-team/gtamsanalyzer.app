//
//  NSMutableNumber.h
//  dataBoiler
//
//  Created by matthew on Wed Sep 18 2002.
//  Copyright (c) 2002 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "utils.h"
#define FLOATTYPE 1
#define INTTYPE 0
@interface NSMutableNumber : NSObject {
    int _sign, _type, _exp;
    int _iValue;
    int _decPlaces;
    float _fValue;
}
-(id) initWithInt: (int) value;
-(id) initWithFloat: (float) value;
-(id) initWithString: (NSString *) value;
-(id) initWithFloat: (float) value withDecPlaces: (int) dp;
-(void) setStringValue: (NSString *) value;
-(NSString *) description;
-(void) inc;
-(void) dec;
-(void) add: (NSMutableNumber *) what;
-(void) addInt: (int) what;
-(void) addFloat: (float) what;
-(void) addString: (NSString *) what;
//converters
-(void) toInt;
-(void) toFloat;
-(void) negate;
-(void) zeroInt;
-(void) zeroFloat;
//accessors
-(int) type;
-(int) decPlaces;
-(float) floatValue;
-(int) intValue;
-(void) setDecPlaces: (int) n;
-(void) setInt: (int) what;
-(void) setFloat: (float) what;
-(void) setFloat: (float) what decPlaces: (int) howMany;

@end
