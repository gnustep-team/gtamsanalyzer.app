//
//  coderec.h
//  CocoaTams
//
//  Created by matthew on Mon Apr 15 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyDocument.h"
#import "MWFile.h"

#define DATARUN 0
#define REPEATRUN 1
#define SECTIONRUN 2

@interface coderec : NSObject {
    NSMutableString *theName;
    NSMutableString *theCoder;
    NSMutableString *record;
    NSMutableString *comment;
    NSMutableDictionary *affiliatedInfo;
    NSMutableArray *sectionCodes;
    BOOL openRun;
    BOOL hasInfo;
    unsigned long bloc, eloc, lineloc;
    unsigned lineStart, lineEnd;
    MWFile *doc;
    int runType;

}
- (id) init;
- (void) dealloc;
- (void) setName: (NSString *) myname;
- (void) setCoder: (NSString *) myname;
- (void) setRecord: (NSString *) acode;
- (void) setComment: (NSString *) acomment;
-(void) setAffiliatedInfo: (NSMutableDictionary *) ai;
-(void) addAffiliatedInfo: (NSMutableDictionary *) ai;
-(NSMutableDictionary *) affiliatedInfo;
-(void) addSectionCodes: (NSArray *) who;

- (void) setOpen: (BOOL) isopen;
-(BOOL) hasInfo;
- (NSMutableString *) theName;
- (NSMutableString *) theCoder;
- (NSMutableString *) record;
- (NSMutableString *) comment;
-(NSMutableArray *) sectionCodes;
- (MWFile *) doc;
- (void) setDoc: (MWFile *) who;
- (int) runType;
- (void) setRunType: (int) what;
- (BOOL) openRun;
- (void) addChar: (unichar) c;
- (void) addString: (NSString *) ss;
- (void) appendString: (NSString *) ss;
- (void) setBegin: (unsigned long) b;
-(void) setEnd: (unsigned long) e;
- (unsigned long) beginLoc;
-(unsigned long) endLoc;
-(void) setLineStart: (unsigned) what;
-(void) setLineEnd: (unsigned) what;
-(unsigned) lineStart;
-(unsigned) lineEnd;

@end
