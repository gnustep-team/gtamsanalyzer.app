/* 
 * AppController.h created by phr on 2000-08-27 11:38:59 +0000
 *
 * GNUstep Application Controller
 *
 * Created with ProjectCenter - http://www.gnustep.org
 *
 * $Id: AppController.h,v 1.9 2002/01/02 12:13:06 probert Exp $
 */

#import <AppKit/AppKit.h>

@interface AppController : NSObject
{
}

+ (void)initialize;

- (id)init;
- (void)dealloc;

- (void)awakeFromNib;

- (void)applicationDidFinishLaunching:(NSNotification *)notif;

- (BOOL)applicationShouldTerminate:(id)sender;
- (void)applicationWillTerminate:(NSNotification *)notification;

- (BOOL)application:(NSApplication *)application openFile:(NSString *)fileName;

- (void)showPrefPanel:(id)sender;
- (void)showInfoPanel:(id)sender;

@end
