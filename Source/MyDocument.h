//
//  MyDocument.h
//  TAMS Edit
//
//  Created by matthew on Thu Apr 18 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//
#define DATATYPE 1
#define RESULTTYPE 2

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "toolBarDelagate.h"
#import "MWFile.h"
#import "tams.h"
//#import "myProject.h"

@interface MyDocument : NSDocument
{
    //kludge for flags
    int rawF, exactF, emptyF, simpleF, sectionF, regexCharF, nonRegexCharF;
    int rtfFlag;
    
    //real variables
    int myDocType;
    IBOutlet NSTextView *theDoc;
    IBOutlet NSTableView *theCodeList;
    IBOutlet NSTableView *searchCodeList;
    IBOutlet NSTextField *theNewCode;
    IBOutlet NSTabView *theTabs;
    IBOutlet NSWindow *myWindow;
    IBOutlet id myToolbar;
    IBOutlet id coderIDView;
    IBOutlet id initFileSearchSwitch;
    IBOutlet id movieTitle;
    IBOutlet id myColorWell;
    IBOutlet id inheritedColorSwitch;
    IBOutlet id rawButton;
    IBOutlet id exactButton;
    IBOutlet id emptyButton;
    IBOutlet id simpleButton;
    IBOutlet id sectionButton;
    
    IBOutlet id codeButton;
    
    IBOutlet NSTextView *codeInfo;
    IBOutlet NSTextField *newCodeName;
    IBOutlet NSWindow *codeInfoSheet;
    IBOutlet id codeColorMenu;

    IBOutlet id widthSheet;
    IBOutlet id wsWidthSize;
    IBOutlet id wsStartNumber;
    IBOutlet id wsBlankBreakSwitch;
    IBOutlet id wsCountEmptyLineSwitch;
    IBOutlet id commentView;
    IBOutlet id commentSheet;
    
    id gWorkBench;
    MWFile *myFID;
    IBOutlet id rateMenu;
    NSMutableString *codeInfoStr;
    BOOL dirty;
    NSMutableString *myData;
    NSData *dataFromFile;
    NSMutableArray *hotCodeList;
    
    //for searching
    IBOutlet NSTextField *theLimitString;
    IBOutlet NSTextField *theWindowName;
    
    //for comments
    IBOutlet id setCommentSheet;
    IBOutlet id setCommentMenu;
    IBOutlet id setCommentField;
    
    //for code sets
    IBOutlet id codeSetSheet;
    IBOutlet id codeSetName;
    
    
    //for bookmarks
    NSMutableArray *bookmarkVault;
    NSMenuItem *rebuildBMenuItem;
    NSMutableDictionary *savedFormat;
    //for macros
    NSMutableArray *macroArray;
    //for av
#ifdef MULTIMEDIA
    IBOutlet NSMovieView *myMovieView;
     NSMovie *myMovie;
     IBOutlet id stopStartButton;
     IBOutlet id timeField;
     NSTimer *myTimer;
     BOOL jagFlag;
     long duration;
#else
     id myMovie;
#endif
}
-(int) getDocType;
-(NSString *) getTitle;
-(NSString *) theLimitString;
-(NSString *) theWindowName; 
-(int) rawF;
-(int) exactF;
-(int) emptyF;
-(int) simpleF;
-(int) sectionF;
-(int) regexCharF;
-(int) nonRegexCharF;
-(void) addCode: (NSString *) what from: (int) bwhere to: (int) ewhere from: (id) who withDef: (NSString *) myDef;
-(void) addCode: (NSString *) what from: (int) bwhere to: (int) ewhere from: (id) who;
-(void) recode: (NSString *) what first: (int) bwhere last: (int) ewhere from: (id) who;
-(void) recode: (NSString *) what first: (int) bwhere last: (int) ewhere from: (id) who withDef: (NSString *) myDef;
-(NSArray *) getHotCodeList;
-(NSString *) stringFrom: (int) b to: (int) e;
-(void) recode: (NSString *) what comment: (NSString *)cmt  first: (int) bwhere last: (int) ewhere from: (id) who withDef: (NSString *) myDef;
-(void) deleteCodeFromResultStartingWith: (int) bwhere endingWith: (int) ewhere from: (id) who;
-(void) adjustResultDocsAt: (int) loc forLength: (int) len;

-(BOOL) isNew: (NSString *) what;
-(void) appendString: (NSString *)what;
-(void) setDirty: (BOOL) value;
-(BOOL) dirty;
-(IBAction) rebuildBookmarks: (id) sender;
-(IBAction) okCodeInfo: (id) sender;
-(IBAction) searchForCode: (id) Sender;
-(IBAction) integrateCodes: (id) sender;
-(IBAction) doFindCurrentCode: (id) sender;
-(IBAction) doFindNextCode: (id) sender;
-(IBAction) checkForPairs: (id) sender;
-(IBAction) checkForNested: (id) sender;
-(IBAction) insertTAMSText: (id) sender;
-(IBAction) moveWorkBenchForward: (id) sender;
-(IBAction) addItemToToolbar: (id) sender;
-(IBAction) doRemoveBookmarks: (id) sender;
-(IBAction) doFindSelectedRange: (id) sender;
-(IBAction) doTurnTextToButton: (id) sender;
-(IBAction) doToggleRuler: (id) sender;
-(IBAction) requestCodeDefinition: (id) sender;
-(IBAction) doColorize: (id) sender;
#ifdef MULTIMEDIA
-(IBAction) setRate: (id) sender;
#endif
-(void) setReanalysisState: (BOOL) which;
-(void) setTitleFromFile;
-(void) setGWorkBench: (id) who;
-(id) getGWorkBench;
-(void) setMWFile: (MWFile *) who;
-(MWFile *) getMyMWFile;
-(void) scanForMetas;
-(void) renewBookmarkMenu;
-(void) insertString: (NSString *) what;
-(void) moveForward;
-(IBAction) generateCodeList: (id) Sender;
-(void) selectAndScrollRange: (NSRange) theRange andMoveForward: (BOOL) move;
-(void) displayCode;
-(IBAction) refreshCodeList: (id) Sender;
-(void) updateCodeWindow;
-(void) zapCodeWindow;
-(IBAction) addCode: (id) Sender;
-(IBAction) newCode: (id) Sender;
-(BOOL) isACode: (NSString *) who;

-(IBAction) okCountLines: (id) sender;
-(IBAction) cancelCountLines: (id) sender;

-(IBAction) okCodeWithComment: (id) sender;
-(IBAction) cancelCodeWithComment: (id) sender;
-(IBAction) editCodeSet: (id) Sender;
-(void) scrollToTop;
-(NSMutableArray *) hotCodeList;
-(NSArray *) remakeCodeList;
-(void) assignCode;
-(void) loadtextViewWithData:(NSData *) data;
-(void) setData;
-(NSString *) getData;
-(void) applyCode: (NSString *) aCode;
-(void) applyCode: (NSString *) aCode withComment: (NSString *) comment;
-(BOOL)tableView: (NSTableView *) aTable shouldEditTableColumn: (NSTableColumn *) aCol row: (unsigned) arow;
-(void) sheetDidEnd: (NSWindow *) mySheet returnCode: (int) returnCode contextInfo: (void *) contextInfo;
-(BOOL) isMyCode: (NSString *) who;
-(BOOL) isValidCode: (NSString *) who;
-(NSString *) codersList;
-(void) performClose;
-(void) printShowingPrintPanel: (BOOL) flag;
-(NSTextView *) myTextView;
//movies
#ifdef MULTIMEDIA
-(IBAction) startMovie: (id) sender;
-(IBAction) stopMovie: (id) sender;
-(IBAction) openMovie: (id) sender;
-(IBAction) insertMovieTime: (id) sender;
-(IBAction) setMovieTime: (id) sender;
-(IBAction) backSpaceMovieTime: (id) Sender;
-(void) loadMediaFile: (NSString *) who;

-(IBAction) fwdFrame: (id) sender;
-(IBAction) bkFrame: (id) sender;
-(IBAction) gotoMovieEnd: (id) sender;
-(IBAction) gotoMovieStart: (id) sender;
-(IBAction) clearMediaFileTags: (id) sender;
-(IBAction) speedUp: (id) sender;
-(IBAction) slowDown: (id) sender;
-(void) endTimer;
#endif
-(IBAction) selectColorFromWell:(id)Sender;
-(IBAction) selectColorFromSwitch:(id) sender;
-(IBAction) rebuildDocMacros: (id) sender;
-(IBAction) handleDocMacro: (id) sender;
-(void) addMacroItem: (NSString *) ss code: (BOOL) aCode;
-(IBAction) okSetComment: (id) sender;
-(IBAction) cancelSetComment: (id) sender;

-(IBAction) cancelNameCodeSet: (id) Sender;
-(IBAction) okNameCodeSet: (id) Sender;
-(IBAction) doNameCodeSet: (id) Sender;

@end

extern MyDocument *gCurrentDataSource;
