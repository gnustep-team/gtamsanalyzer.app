/* appDelegate */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "prefBoss.h"
#import "codeBrowser.h"
#import "myProject.h"

@interface appDelegate : NSObject
{
    prefBoss* pb;
    codeBrowser *cd;
    IBOutlet NSMenuItem *codeSetMenu;
    IBOutlet NSMenuItem *rsltCodeSetMenu;
    IBOutlet NSMenuItem *rebuildMenuItem;
    IBOutlet NSMenuItem *namedSelectionMenu;
    IBOutlet NSMenuItem *autoSetMenu;
    IBOutlet NSMenuItem *reanalysisMenu;
    IBOutlet NSMenuItem *fileSetMenu;
    IBOutlet NSMenuItem *summReportMenu;
    IBOutlet NSMenuItem *docMacroMenu;
    IBOutlet NSMenuItem *workMenu;
    IBOutlet NSMenuItem *caseMenuItem;
    IBOutlet NSMenuItem *toggleShowTagsMenuItem;
    IBOutlet id delWorkMenu;
    IBOutlet id delWorkPanel;
}
//- (void)applicationDidFinishLaunching:(NSNotification *)aNotification;
- (void)applicationWillFinishLaunching:(NSNotification *)aNotification;
- (NSMenuItem *) rebuildMenuItem;
- (void) doPreference: (id) sender;
- (NSMenuItem *) namedSelectionMenu;
- (NSMenuItem *) autoSetMenu;
-(NSMenuItem *) reanalysisMenu;
-(NSMenuItem *) codeSetMenu;
-(NSMenuItem *) fileSetMenu;
-(NSMenuItem *) summReportMenu;
-(NSMenuItem *) rsltCodeSetMenu;
- (void) doCodeDictionary: (NSMutableDictionary *) cl from: (myProject *) who;
-(NSMenuItem *) docMacroMenu;
-(NSMenuItem *) caseMenuItem;
-(NSMenuItem *) toggleShowTagsMenuItem;
-(IBAction) clearAllWorkItems: (id) sender;

-(IBAction) okRemoveWorkItem: (id) sender;
-(IBAction) removeWorkItem: (id) sender;
-(IBAction) cancelRemoveWorkItem: (id) sender;
-(IBAction) deleteAllWorkItems: (id) sender;
-(IBAction) addWorkItem: (NSString *) path;
@end
