//
//  MWRange.m
//  TEST2
//
//  Created by matthew on Sat Feb 07 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import "MWRange.h"


@implementation MWRange
-(id)initWithRange: (NSRange) rr
{
    [super init];
    r = rr;
    return self;
}
-(void) setLocation: (int) a length: (int) b
{
    r.location = a;
    r.length = b;
}
-(unsigned) location {return r.location;}
-(unsigned) length {return r.length;}
-(unsigned) end {return r.location + r.length;}
-(NSRange) range {return r;}
-(void) setRange: (NSRange) rr{r = rr;}

@end
