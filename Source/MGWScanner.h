//
//  MGWScanner.h
//  TEST2
//
//  Created by matthew on Wed Nov 05 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSScanner(MGWScanner)
-(void) next;
-(void) prev;
@end
