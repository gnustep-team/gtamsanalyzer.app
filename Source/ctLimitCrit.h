//
//  ctLimitCrit.h
//  CocoaTams
//
//  Created by matthew on Mon Apr 15 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "utils.h"
@interface ctLimitCrit : NSObject {
    NSMutableString *name;
    char modif;
    int isnot;
    NSMutableArray *coders;

}
-(id) initWithString: (NSString *) who;
-(id) init;
-(void) dealloc;
-(NSMutableString *) name;
-(void) addCoder: (NSString *) who modifier: (NSString *) how;
-(NSMutableArray *) getCoders;
-(BOOL) areCoders;
-(void) setName: (NSMutableString *) n;
-(NSMutableString *) getName;
-(char) modif;
@end
