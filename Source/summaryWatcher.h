/* summaryWatcher */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface summaryWatcher : NSObject
{
    IBOutlet id compLevelField;
    IBOutlet id sortSwitch;
    IBOutlet id sortMenu;
    IBOutlet id countDupSwitch;
    IBOutlet id projWideSwitch;
    IBOutlet id countBlanksSwitch;
    IBOutlet id resultClass;
    IBOutlet id resultWindow;
    IBOutlet id summFieldPopMenu;
    IBOutlet id summGroupPopMenu;
    IBOutlet id summGroupTable;
    IBOutlet id summGroupTypePopMenu;
    IBOutlet id summName;
    IBOutlet id summPane;
    IBOutlet id summReportPopMenu;
    IBOutlet id summTypePopMenu;
    IBOutlet id summSumFieldPopMenu;
    IBOutlet id summFieldType;
    IBOutlet id summFieldCompLevel;
    NSMutableDictionary *currReport;
    NSMutableArray *currGroups;
    int firstTimeFlag;
    
}
-(IBAction) resetFields: (id) sender;
- (IBAction)addGroup:(id)sender;
- (IBAction)addSummRep:(id)sender;
- (IBAction)cancelSumm:(id)sender;
- (IBAction)delAllGroup:(id)sender;
- (IBAction)delAllSummRep:(id)sender;
- (IBAction)delGroup:(id)sender;
- (IBAction)delSummRep:(id)sender;
- (IBAction)loadReport:(id)sender;
- (IBAction)myAction:(id)sender;
- (IBAction)okSumm:(id)sender;
- (IBAction)suffleDownGroup:(id)sender;
- (IBAction)suffleUpGroup:(id)sender;
-(void) runSummReport: (NSDictionary *) r;
-(NSMutableDictionary *) getCurrReport;
-(IBAction) addSortStackToGroups: (id) sender;
-(NSArray *) sortStack2Group;

-(void) doSummary;

@end
