//
//  tableBrowser.m
//  TamsAnalyzer
//
//  Created by matthew on Sat Jun 15 2002.
//  Copyright (c) 2002 Matthew Weinstein. All rights reserved.
//
#import "tams.h"
#import "tableBrowser.h"
#import "ctQChar.h"
//#import "ctReadChar.h"
#import "xrefTableColumn.h"
#import "utils.h"
#import "tamsutils.h"
#define COLWIDTH 90

@implementation tableBrowser
/* initializers and accessors */
-(id) init
{
    [super init];
    dataType = [[NSMutableString alloc] init];
    hotCodeList = [[NSMutableArray alloc] init];
    //theDData = nil;
    return self;
}

-(void) dealloc
{
    [dataType release];
    [hotCodeList release];
    //if(theDData != nil) [theDData release];
    [super dealloc];
}

-(void) setCharEngine: (TAMSCharEngine *) who
{
    tce = who;
}
-(void) setHotCodeList: (NSMutableArray *) hc
{
    [hotCodeList addObjectsFromArray: hc];
}
-(NSWindow *) window
{
    return myWindow;
}
-(void) printShowingPrintPanel: (BOOL) flag
{
    NSPrintInfo *printInfo = [self printInfo];
    NSPrintOperation *printOp;
    printOp = [NSPrintOperation printOperationWithView: theTable
	printInfo: printInfo];
    [printOp setShowPanels: flag];
    [printOp runOperation];
}

/* Main loop */
- (void)windowControllerDidLoadNib:(NSWindowController *)windowController
{
    [self clearWindow];
    if([dataType isEqualToString: @"xref"] == YES)
	[self buildCrossReference];
    else if([dataType isEqualToString: @"count"] == YES)
	[self buildCount];

}

/* REAL LOOP */
-(void)buildCount
{
    NSMutableDictionary *myMatrix;
    NSNumber *n;
    int nn;
    ctQChar *q;
    theDData = myMatrix = [[NSMutableDictionary alloc] init];
    q = [[ctQChar alloc] init];
    

    while([tce scanNext: q] != ENDOFALLFILES)
    {
	if([q tokentype] == TOKEN)
	{
	    if((n = [theDData objectForKey: [q buff]]) != nil)
	    {
		nn = [n intValue] + 1;
		[theDData setObject: [NSNumber numberWithInt: nn] forKey: [q buff]];
	    }
	    else
		[theDData setObject: [NSNumber numberWithInt: 1] forKey: [q buff]];
	}
    }
    [self setupTable];
    [tce release];
    [self updateChangeCount: NSChangeDone];

}


-(void)buildCrossReference
{
    NSMutableArray *myCol, *zoneList;
    NSMutableDictionary *myMatrix;
    xrefTableColumn *md;
    ctQChar *q;
   
    myCol = [[NSMutableArray alloc] init];
    theDData = myMatrix = [[NSMutableDictionary alloc] init];
    zoneList = [[NSMutableArray alloc] init];
    //set up the matrix
    //[myMatrix setObject: myCol forKey: @"codeList"];
    FORALL(hotCodeList)
    {    
	[myMatrix setObject: [[xrefTableColumn alloc] init] forKey: temp];
    }
    ENDFORALL;
    //do the data run
    
    q = [[ctQChar alloc] init];
    [q retain];
    

    while([tce scanNext:q] != ENDOFALLFILES)
    {
	//if it's a tag
	if([q tokentype] == TOKEN)
	{
	    if([zoneList count])
	    {
		FORALL(zoneList)
		{
		    
		    //MD is the column for a particular code
		    if(md = [myMatrix objectForKey: temp])
			if([md hasName: [q buff]] == YES)
			{
			    [md incName: [q buff]];
			}
			else
			{
			    [md addName: [q buff] withValue: 1];
			}
		    
		    if(md = [myMatrix objectForKey: [q buff]])
			if([md hasName: temp] == YES)
			{
			    [md incName: temp];
			}
			else
			{
			    [md addName: temp withValue: 1];
			}
		} ENDFORALL;
	    }
	    [zoneList addObject: [[q buff] copy]];
	}
	if([q tokentype] == ENDTOKEN)
	{
	    id ss;
	    FORALL(zoneList)
	    {
		if([temp isEqualToString: [q buff]])
		    ss = temp;
	    }
	    ENDFORALL;
	    if(ss) [zoneList removeObject: ss];
	}
    }
    [self setupTable];
    [q release];
    [tce release];
    [self updateChangeCount: NSChangeDone];
}    

/* Document portion */
- (BOOL)validateMenuItem:(NSMenuItem *)anItem
{
    BOOL ans = [super validateMenuItem: anItem];
    if([[anItem title] isEqualToString: @"Save"] == YES)
    {
	return NO;
    }
     if([[anItem title] isEqualToString: @"Save As..."] == YES)
    {
	return NO;
    }
   return ans;

}

- (NSString *)windowNibName {
    // Implement this to return a nib to load OR implement -makeWindowControllers to manually create your controllers.
    return @"FreqCount";
}

- (NSData *)dataRepresentationOfType:(NSString *)type {
    // Implement to provide a persistent data representation of your document OR remove this and implement the file-wrapper or file path based save methods.
    NSMutableData *dd;

    
    
    if([dataType isEqualToString: @"xref"] == YES)
    {
	int i, cnt;
	cnt = [hotCodeList count];
	dd = [[NSMutableData alloc] init];
	//print the top
	FORALL(hotCodeList)
	{
	    DATATAB(dd);
	    DATASTRING(dd, temp);
	}
	ENDFORALL;
	DATANL(dd);
	
	//Go through the hotcode list
	for(i = 0; i < cnt; i++)
	{
	//print the code name
	    DATASTRING(dd, [hotCodeList objectAtIndex: i]);
	//print the data
	    FORALL(hotCodeList)
	    {
		char myNum[50];
		xrefTableColumn *ccc = [theDData objectForKey: temp];
		//get the column
		DATATAB(dd);
		sprintf(myNum,"%d", [ccc cntForName: [hotCodeList objectAtIndex: i]]);
		DATASTRING(dd, [NSString stringWithCString: myNum]); 
		//write the data
	    }
	    ENDFORALL;
	    DATANL(dd);
	}
	return dd;
    }
    if([dataType isEqualToString: @"count"] == YES)
    {
	dd = [[NSMutableData alloc] init];
	FORALL(hotCodeList)
	{
	    DATASTRING(dd, temp);
	    DATATAB(dd);
	    DATASTRING(dd, [[theDData objectForKey: temp] stringValue]);
	    DATANL(dd);
	}
	ENDFORALL;
	return dd;
    }

    return nil;
}

- (BOOL)loadDataRepresentation:(NSData *)data ofType:(NSString *)type {
    // Implement to load a persistent data representation of your document OR remove this and implement the file-wrapper or file path based load methods.
    return YES;
}
/* table portion */
- (int)numberOfRowsInTableView:(NSTableView *)aTableView
{
    if([dataType isEqualToString: @"xref"] == YES)
    {
	return [hotCodeList count];
    }
     if([dataType isEqualToString: @"count"] == YES)
    {
	return [hotCodeList count];
    }
   return 0;

}

- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn: (NSTableColumn *)aTableColumn row:(int)rowIndex
{
    if([dataType isEqualToString: @"xref"] == YES)
    {
	xrefTableColumn *m;
	if([[aTableColumn identifier] isEqualToString: @"Codes"] 
	    == YES)
	{
	    return [hotCodeList objectAtIndex: rowIndex];
	}
	
	if(m = [theDData objectForKey: [aTableColumn identifier]])
	{
	    return [NSNumber numberWithInt: 
		[m cntForName: [hotCodeList objectAtIndex: rowIndex]]];
	}

    }
    if([dataType isEqualToString: @"count"] == YES)
    {
 	if([[aTableColumn identifier] isEqualToString: @"Codes"] 
	    == YES)
	{
	    return [hotCodeList objectAtIndex: rowIndex];
	}
	else
	{
	    NSNumber *n;
	    n = [theDData objectForKey: [hotCodeList objectAtIndex: rowIndex]];
	    if(n) return n;
	    else return [NSNumber numberWithInt: 0];
	}
    
    }
    return @"";

}

/* Data portion */
-(void) setupTable
{
    NSTableColumn *t;
 
    //check for xref
    if([dataType isEqualToString: @"xref"] == YES)
    {
    //get code list
	
    //make it the first column
	t = [[NSTableColumn alloc] initWithIdentifier: @"Codes"];
	[theTable addTableColumn: t];
	[t setWidth: COLWIDTH];
	[[t headerCell] setStringValue: @"Codes"];
    //Add a column for each and
	FORALL(hotCodeList)
	{
	    t = [[NSTableColumn alloc] initWithIdentifier: temp];
		[t setWidth: COLWIDTH];
	    [theTable addTableColumn: t];
	    [[t headerCell] setStringValue: temp];
	}ENDFORALL;
	    
    }
    if([dataType isEqualToString: @"count"] == YES)
    {
	t = [[NSTableColumn alloc] initWithIdentifier: @"Codes"];
	[t setWidth: COLWIDTH];
	[theTable addTableColumn: t];
	[[t headerCell] setStringValue: @"Codes"];
	    t = [[NSTableColumn alloc] initWithIdentifier: @"Count"];
	[t setWidth: COLWIDTH];
	    [theTable addTableColumn: t];
	    [[t headerCell] setStringValue: @"Count"];
	
    
    }
}

-(void) setAData: (NSMutableArray *) ad {theAData = ad;}
-(void) setDData: (NSMutableDictionary *) dd {theDData = dd;}
-(void) setDataType: (NSString *) dt {[dataType setString: dt];}
-(void) clearWindow
{
    NSArray *tc = [theTable tableColumns];
    NSTableColumn *t;
   NSEnumerator *ten;
    
    ten = [tc objectEnumerator];
    while(t = [ten nextObject])
	[theTable removeTableColumn: t];
}
-(IBAction) niceClose: (id) Sender
{
    [myWindow performClose: self];
}

@end
