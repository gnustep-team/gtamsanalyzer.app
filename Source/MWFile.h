//
//  MWFile.h
//  TA2 hot
//
//  Created by matthew on Sat Apr 05 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "MyDocument.h"

@interface MWFile : NSObject {
    NSMutableString *path;
    id fid; //points to the actual NSDocument that is open
    BOOL open, hasFile;

}
/* these 2 are key since they work whether or not the file is open or closed */

- (NSString *) string;
- (id) filePointer;


- (NSString *) path;
- (NSString *) name;
- (NSString *) type;
- (NSString *) string;
- (id) filePointer;
- (BOOL) open;
-(BOOL) isReallyOpen;
-(id) setOpenFakeFileWithWorkBench: (id) wb;

-(void) setOpen: (BOOL) state;
-(void) setHasFile: (BOOL) state;
-(void) setPath: (NSString *) aPath;
-(BOOL) hasFile;
-(id) init;
-(id) initWithPath: (NSString *) aPath;
-(void) setOpenFile: (id) aFid;
-(BOOL) isEqual: (MWFile *) who;
-(BOOL) compare: (MWFile *) who;

@end
