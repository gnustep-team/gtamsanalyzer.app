/*
 *  untitled.h
 *  TAMS Edit
 *
 *  Created by matthew on Thu Apr 18 2002.
 *  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
 *
 */
#define COCOATAMS
#define COCOAVERSION
//#define METROWERKS
//#define MACINTOSH
//#define SYMANTEC

//These are for the parser
#define CHAR 0
#define TOKEN 1
#define ENDTOKEN 2
#define META 3
#define ENDMETA 4
#define ENDOFFILE 5
#define ENDOFALLFILES 6

#define MWCHAR unichar
#define SCHAR '>'
#define TEXTKLUDGE 1
#define SECTIONSEARCH
#define USESXML 1
