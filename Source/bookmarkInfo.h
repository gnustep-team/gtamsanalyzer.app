//
//  bookmarkInfo.h
//  TamsAnalyzer
//
//  Created by matthew on Sun May 12 2002.
//  Copyright (c) 2002 Matthew Weinstein. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface bookmarkInfo : NSObject {
    NSMutableString *identifier;
    int location;
}
-(void) setLocation: (int) l;
-(void) setIdentifier: (NSString *) s;
-(int) bmLocation;
-(NSString *) identifier;
@end
