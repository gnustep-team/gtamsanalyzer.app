//
//  MWTAMSStringCategories.m
//  avtams
//
//  Created by matthew on Sun Mar 21 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import "MWTAMSStringCategories.h"


@implementation NSString(MWTAMSStringCategories)
-(NSString *) copyrelease
{
    return [[self copy] autorelease];
}

-(int) numberOfCodeComponents
{
    return [[self codeComponents] count];
}

-(NSString *) lastCodeComponents: (int) cnt
{
    NSArray *myComponents;
    int n;
    myComponents = [self codeComponents];
    n = [myComponents count];
    if(cnt < n)
    {
        NSArray *myNewSelf;
        myNewSelf = [myComponents subarrayWithRange: NSMakeRange(n - cnt, cnt)];
        return [myNewSelf componentsJoinedByString: @">"];
    }
    else return [self copyrelease];
}

-(NSString *) lastCodeComponent
{
    return [self lastCodeComponents: 1];
}

-(NSString *) firstCodeComponents: (int) cnt
{
    NSArray *myComponents;
    int n;
    myComponents = [self codeComponents];
    n = [myComponents count];
    
    if(cnt < n)
    {
        NSArray *myNewSelf;
        myNewSelf = [myComponents subarrayWithRange: NSMakeRange(0, cnt)];
        return [myNewSelf componentsJoinedByString: @">"];
    }
    else return [self copyrelease];
}    
    
-(NSString *) firstCodeComponent
{
    return [self firstCodeComponents: 1];
}
-(NSArray *) codeComponents
{
    return [self componentsSeparatedByString: @">"];
}

@end
