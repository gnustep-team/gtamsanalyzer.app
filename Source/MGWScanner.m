//
//  MGWScanner.m
//  TEST2
//
//  Created by matthew on Wed Nov 05 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import "MGWScanner.h"


@implementation NSScanner(MGWScanner)
-(void) next
{
    if([self isAtEnd] == NO) [self setScanLocation: [self scanLocation] + 1];
}
-(void) prev
{
    if([self scanLocation] > 0) [self setScanLocation: [self scanLocation] - 1];
}

@end
