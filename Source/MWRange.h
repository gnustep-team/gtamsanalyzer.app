//
//  MWRange.h
//  TEST2
//
//  Created by matthew on Sat Feb 07 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MWRange : NSObject {
    NSRange r;
}
-(id) initWithRange: (NSRange) rr;
-(void) setLocation: (int) a length: (int) b;
-(unsigned) location;
-(unsigned) length;
-(unsigned) end;
-(NSRange) range;
-(void) setRange: (NSRange) rr;
@end
