/*
 *  MWFile.h
 *  TEST2
 *
 *  Created by matthew on Tue Apr 29 2003.
 *  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
 *
 */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

NSString *abs2rel(NSString *rootPath, NSString *myPath);
NSString *rel2abs(NSString *rootPath, NSString *myPath);
BOOL isAbsPath(NSString *who);
