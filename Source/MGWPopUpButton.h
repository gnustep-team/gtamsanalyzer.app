//
//  MGWPopUpButton.h
//  TEST2
//
//  Created by matthew on Wed Oct 22 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import <AppKit/AppKit.h>


@interface NSPopUpButton(MGWPopUpButton)

-(NSString *) titleOfItemWithTag: (int) t;
-(NSMenuItem *) itemWithTag: (int) t;
-(int) tagOfSelectedItem;
-(void) selectItemWithTag: (int) t;
-(void) selectItemWithTagNumber: (NSNumber *) n;
-(unsigned) count;
@end
