//
//  MWTAMSStringCategories.h
//  avtams
//
//  Created by matthew on Sun Mar 21 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString(MWTAMSStringCategories)
-(int) numberOfCodeComponents;
-(NSString *) lastCodeComponents: (int) cnt;
-(NSString *) lastCodeComponent;
-(NSString *) firstCodeComponents: (int) cnt;
-(NSString *) firstCodeComponent;
-(NSArray *) codeComponents;
-(NSString *) copyrelease;

@end
