//
//  MWKappa.h
//  TEST2
//
//  Created by matthew on Thu Aug 14 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "tableBrowser.h"
#import "MWDoubleDictionary.h"

@interface MWKappa : tableBrowser {
   //kappa stuff
    IBOutlet id theMessage;
    IBOutlet id theFileTitle;
    NSMutableString *fileTitles;
    float kappa, effAgSum;
    int totCount, agreements;
    NSMutableDictionary *ef;
    MWDoubleDictionary *myKMatrix;
    NSMutableArray *answers;
    IBOutlet NSTableView *ansTable;
}
-(void) buildKappa;
@end
