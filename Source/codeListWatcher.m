#import "codeListWatcher.h"
#import "MyDocument.h"
#import "myProject.h"

@implementation codeListWatcher
- (int) numberOfRowsInTableView: (NSTableView*) tableView
{
	int n;
	if((n = [[[myOwner getGWorkBench] hotCodeList] count]) > 0)
		NSLog(@"Count = %d\n",n);
	if([myOwner hotCodeList] == nil)NSLog(@"found a hotCodeList: %d\n", [[myOwner hotCodeList] count]);
	else NSLog(@"No hot codelist");

	return  [[myOwner hotCodeList] count];
}
- (id) tableView: (NSTableView *) tableView objectValueForTableColumn: (NSTableColumn *) tableColumn row:(int) row
{
	return [[myOwner hotCodeList] objectAtIndex: row];
}
@end
