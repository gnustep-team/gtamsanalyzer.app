/*
 *  tamsutils.h
 *  TEST2
 *
 *  Created by matthew on Sat May 03 2003.
 *  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
 *
 */
#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "tams.h"
#import "ctQChar.h"
#import "prefBoss.h"
#import "utils.h"
#define INSERTRESULTCODER(X) {NSMutableString *__cn=[[NSMutableString alloc] init]; [__cn setString: [gPrefBoss getCoderID]]; trimNSS(__cn); if([gPrefBoss showCoder] && [gPrefBoss useCoderValue] && [__cn length]) {[X appendString: @" ["]; [X appendString: __cn]; [X appendString: @"]"];} [__cn release];}

#define INSERTCODER(X) {NSMutableString *__cn=[[NSMutableString alloc] init]; [__cn setString: [gPrefBoss getCoderID]]; trimNSS(__cn); if([gPrefBoss useCoderValue] && [__cn length]) {[X appendString: @" ["]; [X appendString: __cn]; [X appendString: @"]"];} [__cn release];}

BOOL isCodeNameLegal(NSString *who);
BOOL isCharLegal(unichar ch);
NSString *extractFirstCode(NSString *what);

void DATASTRING(NSMutableData *W, NSString *X) ;
BOOL isParentOf(NSString *prnt, NSString *chld);
NSString *parentOf(NSString *code);
NSString *terminusOf(NSString *code);
int codeLevel(NSString *code);
NSString *uniqueFileName();
NSString *uniqueTmpFileName();

NSString *stringForQChar(ctQChar *qq, BOOL cflag);
NSString *code2tag(NSString *mycode, BOOL open, BOOL result, NSString *comment);
void DATACHAR(NSMutableData *W, unichar X) ;
void DATATAB(NSMutableData *W);
void DATANL(NSMutableData *W);
NSColor *getColorForInt(int col);
int compCodeByLevel(NSString *c1, NSString *c2);
NSString *codeToLevel(NSString *mycode, int level);
NSCharacterSet *tamsCharacterSet(void);
BOOL isHHMMSS(NSString *tstr);
unsigned hhmmss2sec(NSString *tstr);
NSString *sec2hhmmss(unsigned l);

enum {black, red, yellow, green, blue, ltgray, gray, dkgray, inheritedColor, otherColor};
