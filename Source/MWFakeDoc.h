//
//  MWFakeDoc.h
//  avtams
//
//  Created by matthew on Thu Mar 18 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "tamsutils.h"
#import "myProject.h"

@interface MWFakeDoc : NSObject {
    BOOL openState;
    BOOL isDirty;
    NSRange selRange;
    NSMutableAttributedString *theDoc;
    MWFile *myMWFile;
    myProject *gWorkBench;
    

}
-(void) setWorkBench: (id) wb;
-(id) initWithMWFile: (MWFile *) mw workBench: (myProject *) wb;
-(BOOL) isReal;
-(void) recode: (NSString *) what comment:(NSString *) cmt first: (int) bwhere last: (int) ewhere from: (id) who withDef: (NSString *) myDef;
-(void) recode: (NSString *) what first: (int) bwhere last: (int) ewhere from: (id) who withDef: (NSString *) myDef;
-(void) addCode: (NSString *) what from: (int) bwhere to: (int) ewhere from: (id) who withDef: (NSString *) myDef;
-(MWFile *) getMyMWFile;
@end
