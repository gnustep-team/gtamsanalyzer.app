//
//  MWDoubleDictionary.h
//  TEST2
//
//  Created by matthew on Wed Aug 13 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MWDoubleDictionary : NSObject {
    NSMutableDictionary *root;

}
-(NSObject *) objectForKey1: (NSString *) who1 key2: (NSString *) who2;
-(void) setObject: (NSObject *) what forKey1: (NSString *) who1 key2: (NSString*) who2;
-(void) incKey1: (NSString *) who1 key2: (NSString *) who2;
-(void) decKey1: (NSString *) who1 key2: (NSString *) who2;
-(int) intForKey1: (NSString *) who1 key2: (NSString *) who2;
-(BOOL) key1Exists: (NSString*) who1;
-(BOOL) objectExistsForKey1:  (NSString *) who1 key2: (NSString *) who2;
-(void) setInt: (int) what forKey1: (NSString *) who1 key2: (NSString *) who2;
-(NSArray *) firstKeys;
-(NSArray *) keysForKey1: (NSString *) who1;
-(int) sumForKey2: (NSString *) who2;
-(int) sumForKey1: (NSString *) who1;
-(int) diagSum;
-(unsigned) count;
-(NSArray *) toArray;
-(void) removeAllObjects;
-(void) removeObjectForKey1: (NSString*) who1 key2: (NSString*) who2;

@end
