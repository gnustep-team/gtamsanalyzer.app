//
//  ctUnlimitedRuns.h
//  CocoaTams
//
//  Created by matthew on Mon Apr 15 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "tams.h"
#import "ctQChar.h"

void ctInitRV();
void ctAddOpenChar(ctQChar *);
void ctOpenRun(ctQChar *);
void ctCloseRun(ctQChar *);
void ctCloseAllOpenRuns();
extern NSMutableArray *ctRunVault;
int israw();
int isrepeat(ctQChar *);