/* codeBrowser */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "myProject.h"
@interface codeBrowser : NSWindowController
{
    NSMutableDictionary *cl, *backCL;
    myProject *codeSource;
    IBOutlet id codeActive;
    IBOutlet id codeDef;
    IBOutlet id codeList;
    IBOutlet id codeName;
    IBOutlet id codeColorMenu;
    IBOutlet id codeColorWell;
    IBOutlet id inheritedColorSwitch;
}

-(void) blank;
-(void) setDict: (NSMutableDictionary *) aCL from: (myProject *) who ;
-(IBAction) doClearDef: (id) sender;
-(IBAction) doSaveDef: (id) sender;
-(IBAction) doRestoreDef: (id) sender;
-(IBAction) doDeleteDef: (id) sender;
-(IBAction) doCloseDef: (id) sender;
-(int) doSaveDefinition;
-(IBAction) selectColorFromWell:(id)Sender;
-(IBAction) selectColorFromSwitch:(id) sender;
-(IBAction) removeAllColors: (id) sender;

@end
