/* fileListMgr */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface fileListMgr : NSObject
{
    IBOutlet id myProj;
}
- (int)numberOfRowsInTableView:(NSTableView *)aTableView;
- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex;
- (BOOL)tableView:(NSTableView *)aTableView shouldEditTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex;
@end
