/*
 *  ctReadChar.h
 *  CocoaTams
 *
 *  Created by matthew on Mon Apr 15 2002.
 *  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
 *
 */

#import <Foundation/Foundation.h>
#import "ctQChar.h"
#import "utils.h"
#import "MyDocument.h"

int readnext(ctQChar *qc);
void resetDataStream();
extern NSString *gTheData;


NSArray *hotFileList();
void addArrayToSearchList(NSArray *who);

void initSearchListSystem();
void addFileToSearchList(MyDocument *who);
void clearSearchList();
void startSearch();
void resetDataStream();
extern MyDocument *hotSearchDocument;

void setCharLocation(int loc);
int getCharLocation();
