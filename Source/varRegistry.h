//
//  varRegistry.h
//  TAMS Edit
//
//  Created by matthew on Sun Apr 21 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import <Foundation/Foundation.h>

struct regObject
{
    NSMutableString *key;
    NSNumber *value;
};

@interface varRegistry : NSObject {
    NSMutableDictionary *theVault;
}
-(NSNumber*) find: (NSString*) key;
-(int) findInt: (NSString*) key;
-(unsigned) findUns: (NSString* ) key;
-(void) setKey: (NSString *) key intValue: (int) myValue;
-(void) setKey: (NSString *) key unsValue: (unsigned) myValue;
-(NSMutableDictionary *) getGlobals ;
-(void) setGlobals: (NSDictionary *) who ;

@end
