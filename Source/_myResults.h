//
//  myResults.h
//  TAMS Edit
//
//  Created by matthew on Sun Apr 21 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "MyDocumentSimple.h"
#import "varRegistry.h"
#import "ctQChar.h"
#import "TAMSCharEngine.h"
#import "AGRegex.h"
#import "MWFile.h"

#define SELECT 1
#define SELECTADDITIONAL 2
#define SELECTREVERSE 3
#define SELECTFEWER 4
#define SELECTCODESET 5
#define SELALPHA 1
#define SELALPHACASE 2
#define  SELEQUAL 3
#define SELGT 4
#define SELGTE 5
#define SELLT 6
#define SELLTE 7
#define SELDATE 8
#define SELBEFOREDATE 9
#define SELAFTERDATE 10
#define SELBEFOREEQDATE 11
#define SELAFTEREQDATE 12
#define SELEXACT 13
#define CODEMODENEW 0
#define CODEMODEOLD 1
#define AUTOSETBASE 8
#define SUMMREPORTBASE 1

int nsStringComp(id first, id second, void *key);
int nsMWFileComp(id first, id second, void *key);

@interface myResults : NSDocument 
{
    int myDocType;
    TAMSCharEngine *tce;
    BOOL closePermission;
    BOOL stripTags;
    IBOutlet NSTableView *theResults;
    IBOutlet NSTextView *thisCell;
    IBOutlet id addCodeName;
    IBOutlet id recodeName;
    IBOutlet id myColorWell;
    IBOutlet id inheritedColorSwitch;
    IBOutlet id newCodeName;
    IBOutlet id newCodeDef;
    IBOutlet id codePrompt;
    IBOutlet id dirtySourceFlag;
    IBOutlet id recCount;
    IBOutlet id selectSheet;
    IBOutlet id selectMenu;
    IBOutlet id selectField;
    IBOutlet id selectFloat;
    IBOutlet id selectRegexFlag;
    IBOutlet id dateFormatSheet;
    IBOutlet id dateFormatOther;
    IBOutlet id dateFormatMenu;
    
    IBOutlet id namedSelSheet;
    IBOutlet id namedSelField;
    
    IBOutlet id commentSheet;
    IBOutlet id commentField;
    
    IBOutlet id setMathOpMenu;
    IBOutlet id setMathSetMenu;
    IBOutlet id setMathSheet;
    IBOutlet id limitField;
    IBOutlet id codeColorMenu;
    
    IBOutlet id dotGraphWatcher;
    IBOutlet id exportWatcher;
    MWFile *myMWFID;
    id arBossWindow;
    id gWorkBench;
    int recodeFlag;
    int selectMode;
    NSMutableString *dfString;
    int dfIndex;
    BOOL caseSortFlag;
    BOOL firstUpdateFlag; //kludge so that first time it updates it wont be dirty
    NSMutableDictionary *univVault, *repVault, *normVault;
    NSMutableArray *univList, *repList, *blockList, *fileList, *colList;
    NSMutableString *limitString;
    NSData *mySavedData;
    NSString *savedDataType;
    //NSArray *rootArray;
    varRegistry *myGlobals;
    NSMutableArray *myData;
    NSMutableString *fmode;
    NSMutableArray *markedList;
    MyDocument *dataSource;
    NSMutableString *lgNewCodeName, *lgNewCodeDef;
    int lgNewCodeColor, lgNewCodeMode;
    NSColor *lgNewRealColor;
    NSMutableArray *codersList;
    NSMutableArray *sortStack;
    NSMutableArray *backData;
    NSMutableArray *setStack;
    NSMutableArray *fwdSetStack;   
    NSMutableArray *lastRepList;
    NSMutableArray *innerRepList;
    
    NSMutableString *extraComment;
    NSMutableArray *commentArray;
    
    IBOutlet id backButton;
    IBOutlet id fwdButton;
    
    NSMutableDictionary *namedSelDict, *fieldMap;
    NSMutableString *namedSelSelected;
    NSMutableString *codersString, *innerRepeat;
    NSMutableDictionary *repCont; //has the contingencies for the repeats

    IBOutlet id sel2CodeSetSheet;
    IBOutlet id sel2CodeSetName;
    IBOutlet id selCSExactSwitch;
    IBOutlet id selCSMenu;
    IBOutlet id selCSSheet;
    BOOL exactCodeSetFlag;

    IBOutlet NSWindow *myWindow;
    IBOutlet NSWindow *addCodeWindow;
    IBOutlet NSWindow *recodeWindow;
    IBOutlet NSWindow *newCodeWindow;
    int innerRFlag;
    BOOL savedNamedSel;
    //autoSet
    IBOutlet id autoSetWatcher;
    NSMutableDictionary *autoSets;
    NSMutableArray *autoHistory;
    NSMutableString *autoHistoryStart;
    //summary report
    IBOutlet id summWatcher;
    NSMutableDictionary *summReports;
    int aCodeLevel;
    IBOutlet id codeLevelPane;
    IBOutlet id codeLevelField;
    int commentMode;
    NSMutableString *browseField;
    IBOutlet id browseFieldSheet;
    IBOutlet id browseFieldMenu;
    
    IBOutlet id addRootName;
    IBOutlet id addRootColor;
    IBOutlet id addRootInherited;
    IBOutlet id addRootPane;
    IBOutlet id delRootLevels;
    IBOutlet id delRootPane;
    //for movies
    NSMutableDictionary *mediaDict;
#ifdef MULTIMEDIA
    IBOutlet id mediaSheet;
    IBOutlet id myMovieView;
    NSMovie *myMovie;
    IBOutlet id mediaFileName;
    IBOutlet id mediaTimeCode;
#else
    id myMovie;
#endif
    MWFile *currMWFile;
    NSTimer *myTimer;
    BOOL jagFlag;
    int mediaRow;
    
}
-(MWFile *) currMWFile;
-(void) setCurrMWFile: (MWFile *) who;

-(void) addAutoSet: (NSMutableDictionary *) what withName: (NSString *) who;
-(NSMutableDictionary *) autoSetForName: (NSString *) who;
-(void) removeAutoSetForName: (NSString *) who;
-(BOOL) validAutoSetName: (NSString *) who;
-(NSArray *) allAutoSetNames;

-(IBAction) doAutoSet: (id) sender;
-(void) rebuildAutoSetMenu;
-(IBAction) createAutoSet: (id) sender;
-(IBAction) removeAllAutoSets: (id) sender;
-(NSMutableDictionary *) autoSets;

-(NSArray *) allSetNames;
-(NSMutableArray*) getSetWithName: (NSString*) who;
-(NSMutableArray*) autoHistory;
-(NSMutableString*) autoHistoryStart;
-(NSWindow *) myWindow;
-(NSArray *) columns;
-(void) handleSelect: (int) selectMode selectType: (int) st withString: (NSString *)  compValue withCol: (NSString *) colName floatV: (int) fp regexV: (int) rf startArray: (NSMutableArray*) a1 endArray: (NSMutableArray*) a2;
-(void) handleSet: (int) opType withSet: (NSArray *) mm startArray: (NSMutableArray*) a1 endArray: (NSMutableArray*) a2;
-(NSMutableArray*) getAutoSetWithName: (NSString *) who;
-(BOOL) validSetName: (NSString *) who;

-(IBAction) selectFewerRecs: (id) Sender;
-(void) displaySelData: (int) row;

-(IBAction) doAddCodeOK: (id) sender;
-(IBAction) doAddCodeCancel: (id) sender;
-(IBAction) doNewCode: (id) sender;
-(void) handleDirtySourceFlag: (NSNotification *) notification;
-(void) executeTAMS;
-(int) getDocType;
-(NSString *) getTitle;
-(void) clearTable;
-(BOOL) isRepeat: (NSString *) name;
-(int) global: (NSString *) name;
-(void) setGlobal: (NSString *) name to: (int) val;
-(void) setCharEngine: (TAMSCharEngine *) who;
-(void) handlemeta: (ctQChar *) myc;
- (void) doTamsSearch: (NSString *) aLimString;
-(void) handleUniversal: (NSString *) namelist;
-(void) handleEnd: (BOOL) section flush: (BOOL) f;
//- (BOOL)readFromFile:(NSString *)fileName ofType:(NSString *)type;
//-(BOOL) writeToFile: (NSString *) filename ofType: (NSString *) type;
-(void) addToList: (NSMutableArray *) aList usingString: (NSString *) aString;
-(NSMutableArray *) convertString: (NSString *)  theString onChar: (unichar) breakChar;
-(void) adjustBy: (int) amt startingAt: (int) loc for: (id) who;
//-(void) addCode: (NSString *) what from: (int) bwhere to: (int) ewhere from: (id) who withDef: (NSString *) myDef;
-(BOOL) isInnerRepeat: (NSString *) name;
-(NSMutableArray *) sortStackCopy;

-(NSMutableString *) getComment;
-(void) setComment: (NSString *) what;
-(void) appendComment: (NSString *) what;
-(void) endComment: (NSString *) what;
-(void) endLastComment;
-(void) clearComments;

-(NSMutableString *) limString;
-(void) setGlobalInt: (id) key value: (int) v;
-(IBAction) sortColumn: (id) Sender;
-(IBAction) fetchRow: (id) Sender;
-(IBAction) refreshResults: (id) Sender;
-(IBAction) doMarkAll: (id) sender;
-(IBAction) doUnmarkAll: (id) sender;
-(IBAction) doDeleteMarked: (id) sender;
-(IBAction) doReverseMarked: (id) sender;
-(IBAction) doMark: (id) sender;
-(IBAction) doUnmark: (id) sender;
-(IBAction) doRecode: (id) sender;
-(IBAction) doAddCode: (id) sender;
-(IBAction) doShowComment: (id) sender;
-(BOOL) isMarked: (int) row;
-(IBAction) selectAllRecs: (id) Sender;
-(IBAction) selectAdditionalRecs: (id) Sender;
-(IBAction) selectRecs: (id) Sender;
-(IBAction) doSelect: (id) Sender;
-(IBAction) cancelSelect: (id) Sender;
-(IBAction) cancelSetDateFmt: (id) Sender;
-(IBAction) okSetDateFmt: (id) Sender;
-(IBAction) doSetDateFmt: (id) Sender;
-(void) doContWithDict: (NSMutableDictionary *) theVault;
-(IBAction) clearNamedSelMenu: (id) Sender;
-(void) setupTables;
-(void) setDataSource: (NSString *) theData;
//-(BOOL) writeToFile: (NSString *) filename ofType: (NSString *) type;
-(BOOL)tableView: (NSTableView *) aTable shouldEditTableColumn: (NSTableColumn *) aCol row: (unsigned) arow;
- (void)tableViewSelectionDidChange:(NSNotification *)aNotification;
- (void)tableView:(NSTableView*)tableView didClickTableColumn:(NSTableColumn *)tableColumn;
-(void) displaySelData;
-(id) getGWorkBench: (id) who;
-(void) setGWorkBench: (id) who;
-(void) setMWFile: (MWFile *) who;
-(MWFile *) getMWFile;
-(IBAction) moveWorkBenchForward: (id) sender;
- (void) moveForward;
-(IBAction) showCodeBrowser: (id) Sender;
-(void) setOriginalStateString;
-(void) addStateString: (NSString *) what;
-(void) setStateString: (NSString *) what;
-(BOOL) isBlocked: (NSString *) name;
-(void) setCharEngineWithSource: (id) src andFileList: (NSArray *) myList;
-(BOOL) isAutoSet: (NSString *) who;
-(IBAction) okNamedSelection: (id) Sender;
-(IBAction) cancelNamedSelection: (id) Sender;

-(IBAction) okCodeLevel: (id) sender;
-(IBAction) cancelCodeLevel: (id) sender;
-(NSArray *) allSummReports;
-(NSArray *) mySummReports;
-(void) delSummReport:(NSString *) who;
-(void) addSummReport:(NSString *) who report: (NSDictionary *) r global: (BOOL) g;
-(BOOL) isSummReport: (NSString *) who;
-(NSDictionary *) summReport: (NSString *) who;
-(int) reportScope: (NSString *) who; // 1 = local 2 = global 0 = nonexistant
-(NSMutableString *) dateFormat;
-(NSMutableArray *) hotData;
-(NSMutableArray *) allData;
-(void) rebuildSummReportMenu;
-(IBAction) doFillName: (id) sender;

-(NSArray *) tableColumns;
-(int) fieldType: (NSString *) name;
-(int) codeLevel;

-(void) zeroSetStack;
-(IBAction) popSetStack: (id) sender;
-(void) pushSetStack;
-(IBAction) popFwdSetStack: (id) sender;

-(IBAction) doBrowseField: (id) sender;
-(IBAction) okBrowseField: (id) sender;
-(IBAction) cancelBrowseField: (id) sender;
-(IBAction) selectColorFromWell:(id)Sender;
-(IBAction) selectColorFromSwitch:(id) sender;
-(IBAction) okSetMath: (id) Sender;
-(IBAction) cancelSetMath: (id) Sender;

-(IBAction) doAppendCommentToMarked: (id) sender;
-(IBAction) doAddComment: (id) sender;
-(IBAction) okAddComment: (id) sender;
-(IBAction) cancelAddComment: (id) sender;
-(IBAction) cancelSel2CodeSet: (id) Sender;
-(IBAction) okSel2CodeSet: (id) Sender;
-(IBAction) doSelectCodeSetDialogue: (id) Sender;
-(IBAction) selCSSelect: (id) Sender;
-(IBAction) selCSSelectMore: (id) Sender;
-(IBAction) selCSSelectLess: (id) Sender;
-(IBAction) selCSCancel: (id) Sender;
-(BOOL) selectCodeSetWithName:(NSString *) who withWarnings: (BOOL) w pushStack: (BOOL) ps type: (int) tp exact: (BOOL) ex;
-(IBAction) okAddRoot: (id) sender;
-(IBAction) cancelAddRoot: (id) sender;
-(IBAction) doAddRootCode: (id) sender;
-(IBAction) toggleAddRootColor: (id) sender;
-(IBAction) okDelRoot: (id) sender;
-(IBAction) cancelDelRoot: (id) sender;
#ifdef MULTIMEDIA
-(IBAction) doMediaPlayer: (id) sender;
-(IBAction) exitMediaPlayer: (id) Sender;
-(IBAction) jumpMediaForward: (id) sender;
-(IBAction) jumpMediaBack: (id) sender;
-(IBAction) restartMedia: (id) sender;
#endif
@end
//extern myResults *gCurrentTAMS;
