//
//  MGWMyDocumentToolbar.m
//  avtams
//
//  Created by matthew on Tue Mar 09 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import "MGWMyDocumentToolbar.h"
#import "tams.h"
#import "tamsUtils.h"
#import "utils.h"
NSString *MGWDTBSave=@"Save...";
NSString *MGWDTBWorkbench=@"Workbench";

NSString *MGWDTBRefresh = @"Refresh";
NSString *MGWDTBFindRecord =@"Find record";
NSString *MGWDTBSortUpAZ =@"Sort A-Z";
NSString *MGWDTBSortUpAZWithin =@"Sort A-Z within";
NSString *MGWDTBSmartSortUp =@"Sort";
NSString *MGWDTBSelectAll =@"Select all";
NSString *MGWDTBSelect =@"Select...";
NSString *MGWDTBSelectReverse =@"Select reverse";
NSString *MGWDTBSelectLess =@"Remove from selection...";

NSString *MGWDTBMark =@"Mark";
NSString *MGWDTBUnmark =@"Unmark";
NSString *MGWDTBAddCode =@"Add code...";
NSString *MGWDTBRecode =@"Recode...";
NSString *MGWDTBAutoset =@"Autoset...";
NSString *MGWDTBDataSummary =@"Data summary...";
NSString *MGWDTBExportData =@"Export data...";
#define MWSTRINGCOMP(X,Y) [X isEqualToString: Y]


@implementation MGWMyDocumentToolbar
-(void) dealloc
{
    [myButtons dealloc];
    [allButtons dealloc];
}
-(id) init
{
    [super init];
    myButtons = [[NSMutableArray alloc] init];
    [myButtons addObject: NSToolbarCustomizeToolbarItemIdentifier];
#ifdef COCOAVERSION
    [myButtons addObject: NSToolbarSeparatorItemIdentifier];
#endif
    [myButtons addObject: MGWDTBSave];
    [myButtons addObject: MGWDTBWorkbench];
#ifdef COCOAVERSION
    [myButtons addObject: NSToolbarSeparatorItemIdentifier];
    
#endif
    allButtons = [[NSMutableArray alloc] init];
    
    [allButtons addObject: NSToolbarCustomizeToolbarItemIdentifier];
    [allButtons addObject: NSToolbarSpaceItemIdentifier];
    [allButtons addObject: NSToolbarFlexibleSpaceItemIdentifier];
#ifdef COCOAVERSION
    [allButtons addObject: NSToolbarSeparatorItemIdentifier];
#endif
    
    [allButtons addObject: MGWDTBSave];
    [allButtons addObject: MGWDTBWorkbench];
    return self;
}
- (NSToolbarItem *)toolbar:(NSToolbar *)toolbar
     itemForItemIdentifier:(NSString *)itemIdentifier
 willBeInsertedIntoToolbar:(BOOL)flag
{
    NSToolbarItem *who;
    if(MWSTRINGCOMP(itemIdentifier, MGWDTBRefresh))
        who = [self itemWithID: MGWDTBRefresh image: @"refresh.tif" action: @selector(refreshResults:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBFindRecord))
            who = [self itemWithID: MGWDTBFindRecord image: @"findRecord.tif" action: @selector(fetchRow:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBSortUpAZ))
            who = [self itemWithID: MGWDTBSortUpAZ image: @"sortAZ.tif" action: @selector(sortUpAZ:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBSmartSortUp))
            who = [self itemWithID: MGWDTBSmartSortUp image: @"smartSort.tif" action: @selector(smartSort:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBSortUpAZWithin))
            who = [self itemWithID: MGWDTBSortUpAZWithin image: @"sortAZWithin.tif" action: @selector(sortUpAZWithin:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBSelectAll))
            who = [self itemWithID: MGWDTBSelectAll image: @"selectAll3.tif" action: @selector(selectAllRecs:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBSelect))
            who = [self itemWithID: MGWDTBSelect image: @"selectSome.tif" action: @selector(selectRecs:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBSelectReverse))
            who = [self itemWithID: MGWDTBSelectReverse image: @"selectReverse.tif" action: @selector(selectReverseRecs:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBSelectLess))
            who = [self itemWithID: MGWDTBSelectLess image: @"removeFrom.tif" action: @selector(selectFewerRecs:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBMark))
            who = [self itemWithID: MGWDTBMark image: @"markRec.tif" action: @selector(doMark:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBUnmark))
            who = [self itemWithID: MGWDTBUnmark image: @"unmarkRec.tif" action: @selector(doUnmark:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBAddCode))
            who = [self itemWithID: MGWDTBAddCode image: @"addCode.tif" action: @selector(doAddCode:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBRecode))
            who = [self itemWithID: MGWDTBRecode image: @"reCode2.tif" action: @selector(doRecode:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBAutoset))
            who = [self itemWithID: MGWDTBAutoset image: @"autoSet.tif" action: @selector(createAutoSet:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBDataSummary))
            who = [self itemWithID: MGWDTBDataSummary image: @"dataSumm2.tif" action: @selector(runSummaryReport:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBSave))
            who = [self itemWithID: MGWDTBSave image: @"Save.tif" action: @selector(saveDocument:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBWorkbench))
            who = [self itemWithID: MGWDTBWorkbench image: @"wbicn.tif" action: @selector(moveWorkBenchForward:)];
    else
        if(MWSTRINGCOMP(itemIdentifier, MGWDTBExportData))
            who = [self itemWithID: MGWDTBExportData image: @"export.tif" action: @selector(exportData:)];
    
    else
        who = [[NSToolbarItem alloc] initWithItemIdentifier: itemIdentifier];
    
    return [who autorelease];  
}

- (NSArray *)toolbarAllowedItemIdentifiers:(NSToolbar*)toolbar
{
    
    return allButtons;
}
- (NSArray *)toolbarDefaultItemIdentifiers:(NSToolbar*)toolbar
{
    //return myButtons;
    return myButtons;
    
}
-(void) clearButtons
{
    [allButtons removeAllObjects];
}
-(BOOL) isButton: (NSString *) who
{
    NSArray *theList;
    theList = [myToolBar items];
    FORALL(theList)
    {
	if([who isEqualToString: [temp itemIdentifier]] == YES)
	    return YES;
    }
    ENDFORALL;
    return NO;
}
- (void)setupToolbar
{
    NSToolbar *toolbar = [[NSToolbar alloc] initWithIdentifier: @"DocumentWindowToolbar"];// uniqueString()];
    [toolbar autorelease];
    [toolbar setDelegate:self];
    [toolbar setAllowsUserCustomization:YES];
    [toolbar setAutosavesConfiguration: YES];
    [mainWindow setToolbar: toolbar];
    myToolBar = toolbar;
}
-(NSToolbarItem *) itemWithID: (NSString *) itemID image: (NSString *) fn action: (SEL) act
{
    NSToolbarItem *item = [[NSToolbarItem alloc]
    initWithItemIdentifier:itemID];
    [item setLabel: itemID];
    [item setPaletteLabel:itemID];
    [item setImage:[NSImage imageNamed: fn]];
    [item setTarget:theDocument];
    [item setAction: act];	
    return item;
}
-(void) awakeFromNib
{
    [self setupToolbar];
    
}

@end
