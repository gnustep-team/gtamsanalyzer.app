//
//  MGWPopUpButton.m
//  TEST2
//
//  Created by matthew on Wed Oct 22 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import "MGWPopUpButton.h"


@implementation NSPopUpButton(MGWPopUpButton)
-(unsigned) count
{
    return [self numberOfItems];
}
-(NSString *) titleOfItemWithTag: (int) t
{
    return [[self itemWithTag: t] title];
}
-(NSMenuItem *) itemWithTag: (int) t
{
    return [self itemAtIndex: [self indexOfItemWithTag: t]];
}

-(int) tagOfSelectedItem
{
    return [[self selectedItem] tag];
}
-(void) selectItemWithTag: (int) t
{
    [self selectItem: [self itemWithTag: t]];
}

-(void) selectItemWithTagNumber: (NSNumber *) n
{
    [self selectItemWithTag: [n intValue]];
}


@end
