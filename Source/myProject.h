//
//  MyDocument.h
//  TA2 hot
//
//  Created by matthew on Sat Apr 05 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "MyDocument.h"
#import "myProject.h"
#import "myResults.h"
#define ABSPATHMODE 0
#define RELPATHMODE 1
#define LOCPATHMODE 2
#define SETMODEFILEs 0
#define SETMODECODEs 1

@interface myProject : NSDocument
{
    int rawF, emptyF, exactF;
    int simpleF, sectionF, regexCharF, nonRegexCharF;
    int uniqueNumber;
    IBOutlet id searchListMenu;
    IBOutlet id searchListSheet;
    IBOutlet id searchListNameField;
    IBOutlet id emptyButton;
    IBOutlet id exactButton;
    IBOutlet id rawButton;
    IBOutlet id simpleButton;
    IBOutlet id sectionButton;
    IBOutlet id searchTypeMenu;
    MWFile *initFile;
    IBOutlet id codeListView;
    IBOutlet id codeSourceView;
    IBOutlet id codeSourceBox;
    IBOutlet id fileListView;
    IBOutlet id codeFileName;
    IBOutlet id myWindow;
    IBOutlet id searchListView;
    IBOutlet id searchStringView;
    IBOutlet id windowNameView;
    IBOutlet id coderIDView;
    IBOutlet id coderIDPrompt;
    IBOutlet id initFileName;
    IBOutlet id projPrefPanel;
    IBOutlet id absPathSwitch;
    IBOutlet id fileCountView;
    IBOutlet id pathModeView;
    IBOutlet id fileListModeView;
    
    IBOutlet id fileModeButton;
    IBOutlet id resultModeButton;
    
    IBOutlet id setName;
    IBOutlet id setMenu;
    
    IBOutlet id searchNameMenu;
    IBOutlet id searchNameAdd;
    IBOutlet id searchNameDelete;
    IBOutlet id searchNameDeleteAll;
    IBOutlet id searchNameSheet;
    IBOutlet id searchNameSheetField;
    IBOutlet id searchNameSheetOK;
    IBOutlet id searchNameSheetCancel;
    
    IBOutlet id codeSetName;
    IBOutlet id codeSetPopMenu;
    IBOutlet id codeSetPopMenu2;
    IBOutlet id codeSetTable;
    IBOutlet id codeSetPane;
    IBOutlet id codeSetSelString;
    IBOutlet id codeSetSelRegex;
    IBOutlet id codeSetSelExact;
    IBOutlet id codeCountView;
    IBOutlet id searchCountView;
    NSMutableArray *codeSetButtons;
    NSMutableDictionary *codeSets, *fileSets, *currSet;
    NSMutableString *codeSetCurrName, *fileSetCurrName, *currName;
   
    IBOutlet id fileSetName;
    IBOutlet id fileSetPopMenu;
    IBOutlet id fileSetPopMenu2;
    IBOutlet id fileSetTable;
    IBOutlet id fileSetPane;
    IBOutlet id fileSetSelString;
    IBOutlet id fileSetSelRegex;
    IBOutlet id fileSetSelExact;
    NSMutableArray *fileSetButtons;
 
    NSMutableDictionary *summReports;
      
    int setMode;
    BOOL codeListDirty;
    NSMutableArray *theHotCodeList;
    BOOL tempCodeSet;
    NSMutableArray *tempCodeList;
    NSMutableArray *tempCodeStack;
    NSMutableArray *openFiles, *searchFiles;

    
    NSMutableDictionary *codeList, *reportDictionary, *namedSearchList, *namedSrch, *autoSetDict;
    NSMutableArray *resultFiles, *fList, *allFiles;
    NSMutableString *savingFileName;
    BOOL reanalysisMode;
    int absPathMode;
    int fileViewMode;
    id closeButtonTarget;
    id closeButtonSel;

}

-(int) rawF;
-(int) exactF;
-(int) emptyF;
-(int) simpleF;
-(int) sectionF;
-(int) regexCharF;
-(int) nonRegexCharF;
-(MWFile *) fileForName: (NSString *) where;
-(IBAction) removeNSL: (id) sender;
-(IBAction) removeAllNSL: (id) sender;
-(IBAction) showCodeBrowser: (id) Sender;
-(BOOL) reanalysisMode;
-(void) setReanalysisMode: (BOOL) value;
-(IBAction) doToggleReanalysisMode: (id) sender;
-(void) rebuildAutoSetMenu;
-(void) handleMakeDirty;

-(MWFile *) fileForPath: (NSString *) where;
-(NSMutableString *) getDefinition: (NSString *) what;
- (void) shoveWindow;
- (void)windowDidUpdate:(NSNotification *)aNotification;
- (BOOL) fileExists: (MWFile *) who;
- (BOOL) isInitFile: (MWFile *) who;
-(MWFile *) getInitFile;
-(void) newCode: (NSString *) what definition: (NSString *) info  color: (int) whatColor;
- (id)initWithWindowNibName:(NSString *)windowNibName;
- (NSMutableArray *) fileList;
- (NSMutableArray *) searchList;
//protocol for result documents
-(NSString *) theLimitString;
-(NSString *) uniqueString;
-(NSString *) theWindowName; 
- (void)windowDidLoad;
-(void) displaySelData;
-(void) front;
-(void) back;
-(void) setDirty;
-(NSArray *) getHotCodeList;
-(NSArray *) allCodeList;
-(BOOL) isCodeActive: (NSString *) who;
-(NSString *) codersList;
-(NSArray *) hotCodeList;
-(void) openMWFile: (MWFile *) who type: (int) what;
-(BOOL) mwFileExists: (MWFile *) who;
-(MWFile *) getMWFile;
-(MWFile *) searchFileName;
-(NSMutableDictionary *) getReports;
-(BOOL) isMyCode: (NSString *) ss;
-(void) broadcastCodeChange: (BOOL) colorize;
-(void) registerResult: (myResults *) who;
-(void) unregisterResult: (MWFile *) who;
-(void) updateFileView;
-(int) absPath;
-(void) newFileWithText: (NSString *) what;
-(IBAction) addHotListToSearch: (id) sender;

-(IBAction) goFileMode: (id) sender;
-(IBAction) goResultMode: (id) sender;
-(IBAction) requestCodeDefinition: (id) sender;

-(IBAction) doFileShuffleUp: (id) sender;
-(IBAction) doFileShuffleDown: (id) sender;
-(IBAction) doSearchShuffleUp: (id) sender;
-(IBAction) doSearchShuffleDown: (id) sender;

-(IBAction) addFileToProj: (id) Sender;
-(IBAction) openSelectedFile: (id) Sender;
-(IBAction) newFileToProj: (id) Sender;
-(IBAction) removeFileFromProj: (id) Sender;

-(IBAction) okPrefPanel: (id) Sender;
-(IBAction) cancelPrefPanel: (id) Sender;

- (IBAction) setInitFile: (id) sender;
- (IBAction)clearSearch:(id)sender;
- (IBAction)doAdd:(id)sender;
- (IBAction)doAddAll:(id)sender;
- (IBAction)doRefresh:(id)sender;
- (IBAction)doRemove:(id)sender;
- (IBAction)doRemoveAll:(id)sender;
- (IBAction)doSearch:(id)sender;
- (IBAction)myAction:(id)sender;

-(void) addCode: (NSString *) what definition: (NSString *) info active: (BOOL) act append: (BOOL) app;
-(IBAction) addNamedSL: (id) Sender;
-(IBAction) okNamedSL: (id) Sender;
-(IBAction) cancelNamedSL: (id) Sender;

-(NSColor *) colorForCode: (NSString *) myCode;
//saving search name

-(IBAction) okNamedSrch: (id) Sender;
-(IBAction) cancelNamedSrch: (id) Sender;
-(IBAction) addNamedSrch: (id) Sender;
-(IBAction) delNamedSrch: (id) Sender;
-(IBAction) delAllNamedSearch: (id) Sender;
-(IBAction) doNamedSrch: (id) Sender;

-(IBAction) codeSetAdd:(id) sender;
-(IBAction) codeSetDel:(id) sender;
-(IBAction) codeSetDelAll:(id) sender;
-(IBAction) codeSetSelAll:(id) sender;
-(IBAction) codeSetSelNone:(id) sender;
-(IBAction) codeSetClose:(id) sender;
-(void) loadAllCodes: (id) sender;
-(void) loadCodeSet: (id) sender;
-(void) loadTempCodeSet: (NSString *) who;
-(void) rebuildCodeSetMenu: (NSMenu *) asmn withSel: (SEL) mysel at: (int) start showState: (BOOL) st;

-(void) rebuildCodeSetMenu;
-(IBAction) editCodeSet: (id) Sender;
-(void) addCodeToCurrSet: (NSString *) who;
-(NSArray *) allCodeSetNames;
-(NSString *) currCodeSet;
-(BOOL) isCodeSet;
-(IBAction) codeSetSelMore: (id) sender;
-(IBAction) codeSetSelLess: (id) sender;
-(IBAction) codeSetSel: (id) sender;
-(void) registerCodeSet: (NSString *) who withCodes: (NSMutableArray *) what force: (BOOL) ff;
-(BOOL) codeSetExists: (NSString *) who;
-(NSArray *) codesInCodeSet: (NSString *) who;
-(void) updateCodeCount;


-(NSArray *) allCodeList;

-(IBAction) fileSetAdd:(id) sender;
-(IBAction) fileSetDel:(id) sender;
-(IBAction) fileSetDelAll:(id) sender;
-(IBAction) fileSetSelAll:(id) sender;
-(IBAction) fileSetSelNone:(id) sender;
-(IBAction) fileSetClose:(id) sender;
-(void) loadAllFiles: (id) sender;
-(void) loadFileSet: (id) sender;
-(void) rebuildFileSetMenu;
-(IBAction) editFileSet: (id) Sender;
-(void) addFileToCurrSet: (MWFile *) who;
-(NSArray *) allFileSetNames;
-(NSString *) currFileSet;
-(BOOL) isFileSet;
-(IBAction) fileSetSelMore: (id) sender;
-(IBAction) fileSetSelLess: (id) sender;
-(IBAction) fileSetSel: (id) sender;
-(NSArray *) hotFileSet;
-(NSArray *) allFileList;
-(void) addFile: (MWFile *) who toSet: (NSString *) s;
-(IBAction) showFileSet: (id) sender;
-(void) loadFileSetWithName: (NSString *) who;
-(void) rebuildFileSetPopMenu;
-(NSArray *) dataFileSetNames;
-(int) hotFileCount;

-(NSArray *) allFileList;


-(NSArray *) mySummReports;
-(void) delSummReport:(NSString *) who;
-(void) addSummReport:(NSString *) who report: (NSDictionary *) r global: (BOOL) g;
-(BOOL) isMySummReport: (NSString *) who;
-(void) rebuildSummReportMenu;



-(void) addAutoSet: (NSMutableDictionary *) what withName: (NSString *) who;
-(NSMutableDictionary *) autoSetForName: (NSString *) who;
-(void) removeAutoSetForName: (NSString *) who;
-(NSArray *) allAutoSetNames;

-(void) setupSearchMode: (int) smode;
-(IBAction) initializeSearchMode: (id) sender;

@end
#define FILEOPEN(X) [gWorkBench fileExists: X]
#define SOURCETYPE 0
#define RESULTFILETYPE 1

