#import "globalCodeMgr.h"
#import "myProject.h"

@implementation globalCodeMgr
- (int)numberOfRowsInTableView:(NSTableView *)aTableView
{
    return [[myProj getHotCodeList] count];
}
- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
    return [[myProj getHotCodeList] objectAtIndex: rowIndex];
}

- (BOOL)tableView:(NSTableView *)aTableView shouldEditTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
    return NO;
}

@end
