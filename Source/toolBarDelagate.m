#import "toolBarDelagate.h"
#import "myProject.h"
#import "utils.h"
#import "prefBoss.h"
#import "tams.h"
#define GSTEPTOOLBAR1 1
NSString *MGWDTBDelPair=@"Del. pair";
NSString *MGWDTBFindNext=@"Next tag";
NSString *MGWDTBFindPrev=@"Prev tag";
NSString *MGWDTBFindMate=@"Find mate";
NSString *MGWDTBColorTags=@"Color";
NSString *MGWDTBDecolorTags=@"Decolor";
NSString *MGWDTBSelectToButton=@"Txt to button";
NSString *MGWDTBSelCodeDef=@"Sel. code def.";
NSString *MGWDTBCodeBrowser=@"Code browser";
NSString *MGWDTBCodeDef=@"Code def.";
NSString *MGWDTBRecolorTags=@"Recolor";

int uniqueNumber = 1;
NSString *uniqueString()
{
	return [NSString stringWithFormat: @"TAMSSTRING %d", uniqueNumber++];
}

@implementation toolBarDelagate
-(void) dealloc
{
    [myButtons dealloc];
    [allButtons dealloc];
    [codePool dealloc];
    [textPool dealloc];
}
-(id) init
{
    [super init];
    codePool = [[NSMutableArray alloc] init];
    textPool = [[NSMutableArray alloc] init];
    myButtons = [[NSMutableArray alloc] init];
#ifdef GSTEPTOOLBAR1
     [myButtons addObject: @"Save..."];
     [myButtons addObject: @"Workbench"];
     [myButtons addObject: MGWDTBFindNext];
     [myButtons addObject: MGWDTBFindPrev];
     [myButtons addObject: MGWDTBFindMate];
     [myButtons addObject: MGWDTBDelPair];
     [myButtons addObject: MGWDTBRecolorTags];
#else
    [myButtons addObject: NSToolbarCustomizeToolbarItemIdentifier];

     [myButtons addObject: NSToolbarSeparatorItemIdentifier];

     [myButtons addObject: @"Save..."];
     [myButtons addObject: @"Workbench"];

     [myButtons addObject: NSToolbarSeparatorItemIdentifier];
#endif
      allButtons = [[NSMutableArray alloc] init];
      
    [allButtons addObject: NSToolbarCustomizeToolbarItemIdentifier];
     [allButtons addObject: NSToolbarSpaceItemIdentifier];
     [allButtons addObject: NSToolbarFlexibleSpaceItemIdentifier];

     [allButtons addObject: NSToolbarSeparatorItemIdentifier];

     [allButtons addObject: @"Save..."];
     [allButtons addObject: @"Workbench"];
     [allButtons addObject: MGWDTBDelPair];
     [allButtons addObject: MGWDTBFindNext];
     [allButtons addObject: MGWDTBFindPrev];
     [allButtons addObject: MGWDTBFindMate];
     [allButtons addObject: MGWDTBRecolorTags];
     [allButtons addObject: MGWDTBColorTags];
     [allButtons addObject: MGWDTBDecolorTags];
     [allButtons addObject: MGWDTBSelectToButton];
     [allButtons addObject: MGWDTBCodeDef];
     [allButtons addObject: MGWDTBSelCodeDef];
    [allButtons addObject: MGWDTBCodeBrowser];
    
     buttonNdx = 1;
    return self;
}
/*
 ghost routines
*/
-(BOOL) isInCodePool: (NSString *)s
{
    FORALL(codePool)
    {
	if([temp isEqualToString: s] == YES) return YES;
    }
    ENDFORALL;
    return NO;
}
-(BOOL) isInTextPool: (NSString *)s
{
    FORALL(textPool)
    {
	if([temp isEqualToString: s] == YES) return YES;
    }
    ENDFORALL;
    return NO;
}

- (NSToolbarItem *)toolbar:(NSToolbar *)toolbar
itemForItemIdentifier:(NSString *)itemIdentifier
willBeInsertedIntoToolbar:(BOOL)flag
{
    NSMenuItem *m;
	NSToolbarItem *item = [[NSToolbarItem alloc] initWithItemIdentifier:itemIdentifier];
	if ( [itemIdentifier isEqualToString:MGWDTBDelPair] ) {
	    [item setLabel:MGWDTBDelPair];
	    [item setPaletteLabel:MGWDTBDelPair];
	    [item setImage:[NSImage imageNamed:@"twoTagBasicNot.tif"]];
	    [item setTarget:theDocument];
	    [item setAction:@selector(doDeleteCodePair:)];	
	}
        else if ( [itemIdentifier isEqualToString: MGWDTBCodeDef] ) {
            [item setLabel:MGWDTBCodeDef];
            [item setPaletteLabel:MGWDTBCodeDef];
            [item setImage:[NSImage imageNamed:@"def.tif"]];
            [item setTarget:theDocument];
            [item setAction:@selector(requestCodeDefinition:)];	
        }
        else if ( [itemIdentifier isEqualToString: MGWDTBCodeBrowser] ) {
            [item setLabel:MGWDTBCodeBrowser];
            [item setPaletteLabel:MGWDTBCodeBrowser];
            [item setImage:[NSImage imageNamed:@"codeBrowser.tif"]];
            [item setTarget:theDocument];
            [item setAction:@selector(showCodeBrowser:)];	
        }
        else if ( [itemIdentifier isEqualToString: MGWDTBFindNext] ) {
            [item setLabel:MGWDTBFindNext];
            [item setPaletteLabel:MGWDTBFindNext];
            [item setImage:[NSImage imageNamed:@"nextTag.tif"]];
            [item setTarget:theDocument];
            [item setAction:@selector(doFindNextCode:)];	
        }
        else if ( [itemIdentifier isEqualToString: MGWDTBFindPrev] ) {
            [item setLabel:MGWDTBFindPrev];
            [item setPaletteLabel:MGWDTBFindPrev];
            [item setImage:[NSImage imageNamed:@"prevTag3.tif"]];
            [item setTarget:theDocument];
            [item setAction:@selector(doFindCurrentCode:)];	
        }
        else if ( [itemIdentifier isEqualToString: MGWDTBRecolorTags] ) {
            [item setLabel:MGWDTBRecolorTags];
            [item setPaletteLabel:MGWDTBRecolorTags];
            [item setImage:[NSImage imageNamed:@"reColor.tif"]];
            [item setTarget:theDocument];
            [item setAction:@selector(recolorTags:)];	
        }
        else if ( [itemIdentifier isEqualToString: MGWDTBFindMate] ) {
            [item setLabel:MGWDTBFindMate];
            [item setPaletteLabel:MGWDTBFindMate];
            [item setImage:[NSImage imageNamed:@"selOtherTag.tif"]];
            [item setTarget:theDocument];
            [item setAction:@selector(doFindMate:)];	
        }
        else if ( [itemIdentifier isEqualToString: MGWDTBColorTags] ) {
            [item setLabel:MGWDTBColorTags];
            [item setPaletteLabel:MGWDTBColorTags];
            [item setImage:[NSImage imageNamed:@"colorTags.tif"]];
            [item setTarget:theDocument];
            [item setAction:@selector(doColorize:)];	
        }
        else if ( [itemIdentifier isEqualToString: MGWDTBSelectToButton] ) {
            [item setLabel:MGWDTBSelectToButton];
            [item setPaletteLabel:MGWDTBSelectToButton];
            [item setImage:[NSImage imageNamed:@"txt2Button.tif"]];
            [item setTarget:theDocument];
            [item setAction:@selector(doTurnTextToButton:)];	
        }
        else if ( [itemIdentifier isEqualToString: MGWDTBSelCodeDef] ) {
            [item setLabel:MGWDTBSelCodeDef];
            [item setPaletteLabel:MGWDTBSelCodeDef];
            [item setImage:[NSImage imageNamed:@"defFromSel.tif"]];
            [item setTarget:theDocument];
            [item setAction:@selector(requestCodeDefinitionOfSelection:)];	
        }
        else if ( [itemIdentifier isEqualToString: MGWDTBDecolorTags] ) {
            [item setLabel:MGWDTBDecolorTags];
            [item setPaletteLabel:MGWDTBDecolorTags];
            [item setImage:[NSImage imageNamed:@"decolorTags.tif"]];
            [item setTarget:theDocument];
            [item setAction:@selector(doColorOff:)];	
        }
        else if ( [itemIdentifier isEqualToString:@"Workbench"] ) {
	    [item setLabel:@"Workbench"];
	    [item setPaletteLabel:[item label]];
	    [item setImage:[NSImage imageNamed:@"wbicn.tif"]];
	    [item setTarget:theDocument];
	    [item setAction:@selector(moveWorkBenchForward:)];	
	}
	else if( [itemIdentifier isEqualToString:@"Save..."] ) {
	    [item setLabel:@"Save"];
	    [item setPaletteLabel:[item label]];
	    [item setImage:[NSImage imageNamed:@"Save.tif"]];
	    [item setTarget:theDocument];
	    [item setAction:@selector(saveDocument:)];	
	}

	else if ([self isInCodePool: itemIdentifier] == YES 
            && [theDocument isACode: itemIdentifier] == YES)
	{
	    NSMutableString *mys = [[NSMutableString alloc] init];
	    
	    if([itemIdentifier length] > 10)
	    {
		[mys setString: [itemIdentifier substringToIndex: 3]];
		[mys appendString: @"�"];
		[mys appendString: [itemIdentifier substringFromIndex:  ([itemIdentifier length] - 7)]];
	    }
	    else
		[mys setString: itemIdentifier];
	    [item setLabel: mys];
	    [item setPaletteLabel: mys];
	    [item setImage:[NSImage imageNamed:@"codeicn.tif"]];
	    [item setTarget:self];
	    [item setAction:@selector(setCode:)];	
	    [item setToolTip: [itemIdentifier copy]];
#ifdef TBCONTROLKEY

            if(buttonNdx <= 9)
            {
                m = [item menuFormRepresentation];
                [m setKeyEquivalent: [NSString stringWithFormat: @"%d", buttonNdx]];
                [m setKeyEquivalentModifierMask: NSControlKeyMask];//|NSCommandKeyMask
                buttonNdx++;
            }
#endif
	}	
	else if ([self isInCodePool: itemIdentifier] == YES 
            && [theDocument isACode: itemIdentifier] == NO)
        {
            NSWARNING(@"You've asked to make a button from a not registered code.");
        }
	else if([self isInTextPool: itemIdentifier] == YES)
	{ 
            
	    NSMutableString *mys = [[NSMutableString alloc] init];
	    
	    if([itemIdentifier length] > 10)
	    {
		[mys setString: [itemIdentifier substringToIndex: 8]];
		[mys appendString: @"�"];
	    }
	    else
		[mys setString: itemIdentifier];
	    [item setLabel: mys];
	    [item setPaletteLabel: mys];
	    [item setImage:[NSImage imageNamed:@"metaicn.tif"]];
	    [item setTarget:self];
	    [item setToolTip: [itemIdentifier copy]];
	    [item setAction:@selector(setText:)];
#ifdef TBCONTROLKEY
            if(buttonNdx <= 9)
            {
                m = [[NSMenuItem alloc] initWithTitle: mys action: NULL keyEquivalent:[NSString stringWithFormat: @"%d", buttonNdx]];
                [m setKeyEquivalentModifierMask: NSControlKeyMask];//|NSCommandKeyMask
                [item setMenuFormRepresentation: m];
                buttonNdx++;
            }
#endif
	    [mys release];
	}
	return [item autorelease];  
}
-(IBAction) setCode: (id) Sender
{
    [theDocument applyCode: [Sender itemIdentifier]];

}
-(IBAction) setText: (id) Sender
{
    [theDocument insertString: [Sender itemIdentifier]];
    if([gPrefBoss autoColorValue])
        [theDocument doColorize: nil];
    

}

- (NSArray *)toolbarAllowedItemIdentifiers:(NSToolbar*)toolbar
{

    return allButtons;
}
- (NSArray *)toolbarDefaultItemIdentifiers:(NSToolbar*)toolbar
{
    //return myButtons;
    return myButtons;

}
-(void) clearButtons
{
    [allButtons removeAllObjects];
    [codePool removeAllObjects];
    [textPool removeAllObjects];
}



-(void) addCmd: (NSString *) ss
{
    if([ss isEqualToString: @"cmd::delTagPair"])
        [myToolBar insertItemWithItemIdentifier: MGWDTBDelPair atIndex: [[myToolBar items] count]];
    else if([ss isEqualToString: @"cmd::findNextTag"])
        [myToolBar insertItemWithItemIdentifier: MGWDTBFindNext atIndex: [[myToolBar items] count]];
    else if([ss isEqualToString: @"cmd::findPrevTag"])
        [myToolBar insertItemWithItemIdentifier: MGWDTBFindPrev atIndex: [[myToolBar items] count]];
    else     if([ss isEqualToString: @"cmd::findMate"])
        [myToolBar insertItemWithItemIdentifier: MGWDTBFindMate atIndex: [[myToolBar items] count]];
    else     if([ss isEqualToString: @"cmd::colorTags"])
        [myToolBar insertItemWithItemIdentifier: MGWDTBColorTags atIndex: [[myToolBar items] count]];
    else     if([ss isEqualToString: @"cmd::decolorTags"])
        [myToolBar insertItemWithItemIdentifier: MGWDTBDecolorTags atIndex: [[myToolBar items] count]];
    else     if([ss isEqualToString: @"cmd::selectedTextToButton"])
        [myToolBar insertItemWithItemIdentifier: MGWDTBSelectToButton atIndex: [[myToolBar items] count]];
     else     if([ss isEqualToString: @"cmd::selectedTextCodeDefinition"])
        [myToolBar insertItemWithItemIdentifier: MGWDTBSelCodeDef atIndex: [[myToolBar items] count]];
    else     if([ss isEqualToString: @"cmd::codeBrowser"])
        [myToolBar insertItemWithItemIdentifier: MGWDTBCodeBrowser atIndex: [[myToolBar items] count]];
    else     if([ss isEqualToString: @"cmd::codeDef"])
        [myToolBar insertItemWithItemIdentifier: MGWDTBCodeDef atIndex: [[myToolBar items] count]];
    else     if([ss isEqualToString: @"cmd::recolorTags"])
        [myToolBar insertItemWithItemIdentifier: MGWDTBRecolorTags atIndex: [[myToolBar items] count]];
   
    
}
-(void) addItem: (NSString *) ss
{
    [allButtons addObject: [[ss copy] autorelease]];
    [codePool addObject: [[ss copy] autorelease]];
    //[myButtons addObject: ss];
    [myToolBar insertItemWithItemIdentifier: [ss copy] atIndex: [[myToolBar items] count]];
}
-(void) addText: (NSString *) ss
{
    [allButtons addObject: [[ss copy] autorelease]];
    [textPool addObject: [[ss copy] autorelease]];
    //[myButtons addObject: ss];
    [myToolBar insertItemWithItemIdentifier: [ss copy] atIndex: [[myToolBar items] count]];
}
-(void) addBar
{

    [myToolBar insertItemWithItemIdentifier: NSToolbarSeparatorItemIdentifier atIndex: [[myToolBar items] count]];

}
-(BOOL) isButton: (NSString *) who
{
    NSArray *theList;
    theList = [myToolBar items];
    FORALL(theList)
    {
	if([who isEqualToString: [temp itemIdentifier]] == YES)
	    return YES;
    }
    ENDFORALL;
    return NO;
}
- (void)setupToolbar
{
    NSToolbar *toolbar = [[NSToolbar alloc] initWithIdentifier: @"tamsDocumentToolbar"];//uniqueString()];
    [toolbar autorelease];
    [toolbar setDelegate:self];
    [toolbar setAllowsUserCustomization:YES];
    [toolbar setAutosavesConfiguration: NO];
    [mainWindow setToolbar: toolbar];
    myToolBar = toolbar;
}

-(void) awakeFromNib
{
    [self setupToolbar];

}


@end
