/*
 *  newline.cpp
 *  xtams
 *
 *  Created by matthew on Wed Dec 19 2001.
 *  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
 *
 */
#include "tams.h"
#include "newline.h"
char nlinechar = '\0';
char eorchar = '\n';
int isnewline(char c)
{
    if((c == 0x0a) || (c == 0x0d))
    {
        if(nlinechar == ' ')
            nlinechar = c;
        return 1;
    }
    return 0;
}
extern char eorchar;
void setnewline(int flag)
{
    if(flag == MACNL)
        nlinechar = 13;
    if(flag == UNIXNL)
        nlinechar = 10;
     eorchar = nlinechar;
}

char getnewline(){return nlinechar;}