/* searchListMgr */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface searchListMgr : NSObject
{
    IBOutlet id myProj;
    IBOutlet id mySource;
}
- (int)numberOfRowsInTableView:(NSTableView *)aTableView;
- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex;
- (BOOL)tableView:(NSTableView *)aTableView shouldEditTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex;
@end
