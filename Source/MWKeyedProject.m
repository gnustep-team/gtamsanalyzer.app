//
//  MGWMyDocumentKeyed.m
//  avtams
//
//  Created by matthew on Sat Jun 05 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import "MWKeyedProject.h"
#import "MWFile.h"
#import "myProject.h"
#import "MyDocument.h"
#import "tams.h"
#import "utils.h"
#import "myResults.h"
//#import "ctReadChar.h"
#import "tableBrowser.h"
#import "prefBoss.h"
#import "TAMSCharEngine.h"
#import "appDelegate.h"
#import "MWPath.h"
#import "ctTagInfo.h"
#import "tamsutils.h"
#import "MWKappa.h"
#import "AGRegex.h"

@implementation myProject(MWKeyedProject)
- (NSData *) keyedDataRepresentation
{
    NSMutableArray *fileArray = [NSMutableArray array];
    NSMutableArray *resultArray = [NSMutableArray array];
    NSMutableArray *searchArray = [NSMutableArray array];
    NSString *pPath, *tPath;
    NSMutableData *myDat = [[NSMutableData alloc] init];
    NSKeyedArchiver *myOut = [[NSKeyedArchiver alloc] initForWritingWithMutableData: myDat];
    [myOut setOutputFormat: NSPropertyListXMLFormat_v1_0];
    /*--Put my variables here */
    
    NSLog(@"In keyeddatarep");
    tPath = [self fileName];
    pPath = [savingFileName stringByDeletingLastPathComponent];
    //make an array
    //add to it a version #
    [myOut encodeObject: @"10" forKey: @"version"];

    
    //add to it the search file list
    if([openFiles count]) {
        FORALL(openFiles)
        {
            if([temp path] == nil) continue;
            switch(absPathMode)
            {
                case ABSPATHMODE:
                    [fileArray addObject: [[temp path] stringByStandardizingPath]];
                    break;
                    
                case RELPATHMODE:
                    [fileArray addObject: abs2rel(pPath, [temp path])];
                    break;
                    
                case LOCPATHMODE:
                    [fileArray addObject: [(MWFile *)temp name]];
                    break;
            };
        }
        ENDFORALL;
        [myOut encodeObject: fileArray forKey: @"fileList"];
    }
    
    if([searchFiles count])
    {
        FORALL(searchFiles)
        {
            if([temp path] == nil) continue;
            switch(absPathMode)
            {
                case ABSPATHMODE:
                    [searchArray addObject: [[temp path] stringByStandardizingPath]];
                    break;
                    
                case RELPATHMODE:
                    [searchArray addObject: abs2rel(pPath, [temp path])];
                    break;
                    
                case LOCPATHMODE:
                    [searchArray addObject:  [(MWFile *)temp name]];
                    break;
            };
            
        }
        ENDFORALL;
        [myOut encodeObject: searchArray forKey: @"searchList"];
    }
    //add to it the init file
    if (initFile)
    {
        switch(absPathMode)
        {
            case ABSPATHMODE:
                [myOut encodeObject: [[initFile path] stringByStandardizingPath] forKey: @"init"];
                break;
            case RELPATHMODE:
                [myOut encodeObject: abs2rel(pPath, [initFile path]) forKey: @"init"];
                break;
                
            case LOCPATHMODE:
                [myOut encodeObject: [initFile name] forKey: @"init"];
                break;
                
        };
    }
    else
        [myOut encodeObject: @"" forKey: @"init"];
    
    //add to it the code dictionary
    if([codeList count])
        [myOut encodeObject: codeList forKey: @"codeList"];
    //save the path mode
    switch(absPathMode)
    {
        case ABSPATHMODE:
            [myOut encodeObject: @"A" forKey: @"pathMode"];
            break;
        case RELPATHMODE:
            [myOut encodeObject: @"R" forKey: @"pathMode"];
            break;
            
        case LOCPATHMODE:
            [myOut encodeObject: @"F" forKey: @"pathMode"];
            break;
            
    };
    if([autoSetDict count])
        [myOut encodeObject: [autoSetDict copy] forKey: @"autosets"];
    if([resultFiles count])
    {
        FORALL(resultFiles)
        {
            if([temp path] == nil) continue;
           switch(absPathMode)
            {
                case ABSPATHMODE:
                    [resultArray addObject: [[temp path] stringByStandardizingPath]]; break;
                case RELPATHMODE:
                    [resultArray addObject: abs2rel(pPath, [temp path])]; break;
                case LOCPATHMODE:
                    [resultArray addObject: [(MWFile *)temp name]]; break;
            };
            
        }
        ENDFORALL;
        [myOut encodeObject: resultArray forKey: @"resultList"];
    }
    if([[self getSaveableNamedSearchList] count])
        [myOut encodeObject: [self getSaveableNamedSearchList] forKey: @"namedSearchList"];
    if([searchListMenu numberOfItems] > 0)
        [myOut encodeObject: [[[searchListMenu selectedItem] title] copy] forKey: @"selectedNamedSearchList"];
    else
        [myOut encodeObject: @"" forKey: @"selectedNamedSearchList"];
    if([namedSrch count])
        [myOut encodeObject: namedSrch forKey: @"namedSearch"];
    if ([codeSets count])
    {
        [myOut encodeObject: [[codeSets copy] autorelease] forKey: @"codeSets"];
    }
    
    if([[self fileSetsForSaving] count])
        [myOut encodeObject: [self fileSetsForSaving] forKey: @"fileSets"];
    
    [myOut encodeObject: [[fileSetCurrName copy] autorelease] forKey: @"currFileSet"];
    if(tempCodeSet)
        [myOut encodeObject: [NSString stringWithString: @""] forKey: @"currCodeSet"];
    else
        [myOut encodeObject: [[codeSetCurrName copy] autorelease] forKey: @"currCodeSet"];
    if([summReports count])
        [myOut encodeObject: summReports forKey: @"summReport"];
        
    /*end block */
    [myOut finishEncoding];
    return myDat;
}

#define IFSAVEDKEY(X) if((testObj = [masterArray decodeObjectForKey: X]) != nil)

- (BOOL)loadKeyedData:(NSData *)data 
{
    // Insert code here to read your document from the given data.  You can also choose to override -loadFileWrapperRepresentation:ofType: or -readFromFile:ofType: instead.
    NSKeyedUnarchiver * masterArray;
    NSArray *fileList;
    MWFile *sfile;
    id testObj;
    
    NSString *tPath;//= [self fileName];
        NSString    *pPath = [[self fileName] stringByDeletingLastPathComponent];
        int vers;
        masterArray = [[NSKeyedUnarchiver alloc] initForReadingWithData: data];
            
        tPath = [self fileName];
        vers = [[masterArray decodeObjectForKey: @"version"] intValue];
        if((testObj = [masterArray decodeObjectForKey: @"autosets"]) != nil)
            [autoSetDict addEntriesFromDictionary: testObj];
        IFSAVEDKEY(@"pathMode")
        {
            if([testObj isEqualToString: @"A"])
                absPathMode = ABSPATHMODE;
            else if([testObj isEqualToString: @"R"])
                absPathMode = RELPATHMODE;
            else  if([testObj isEqualToString: @"L"])
                absPathMode = LOCPATHMODE;
            
            else  if([testObj isEqualToString: @"F"])
                absPathMode = LOCPATHMODE;
        }
        else absPathMode = ABSPATHMODE;

        IFSAVEDKEY(@"resultList")
        {
            fileList = testObj;
            FORALL(fileList)
            {
                if(isAbsPath(temp))
                {
                    MWFile *mwf =[[MWFile alloc] initWithPath: [temp stringByStandardizingPath]];
                    [resultFiles addObject: mwf];
#ifdef FIXRESULTS
                    [self addFile: mwf toSet: @"Results"];
#endif
                }
                else
                {
                    MWFile *mwf ;
                    if([temp length] == 0) continue;
                    if([temp isEqualToString: @""]) continue;
                    mwf = [[MWFile alloc] initWithPath: rel2abs(pPath, temp)];
                    [resultFiles addObject: mwf];
#ifdef FIXRESULTS
                    [self addFile: mwf toSet: @"Results"];
#endif
                }
            }
            ENDFORALL;
        }
        
        IFSAVEDKEY(@"fileList")
        {
            fileList = [masterArray decodeObjectForKey: @"fileList"];
            FORALL(fileList)
            {
                if(isAbsPath(temp))
                {
                    MWFile *mwf = [[MWFile alloc] initWithPath: [temp stringByStandardizingPath]];
                    [openFiles addObject: mwf];
                    [allFiles addObject: mwf];
                }
                else
                {
                    MWFile *mwf;
                    if([temp length] == 0) continue;
                    if([temp isEqualToString: @""]) continue;

                    
                    mwf=[[MWFile alloc] initWithPath: rel2abs(pPath, temp)];
                    
                    [openFiles addObject: mwf];
                    [allFiles addObject: mwf];
                }
            }
            ENDFORALL;
        }
        IFSAVEDKEY(@"searchList")
        {
            fileList = [masterArray decodeObjectForKey: @"searchList"];
            FORALL(fileList)
            {
                if([temp length] == 0) continue;
                if([temp isEqualToString: @""]) continue;
                if(isAbsPath(temp))
                    sfile = [self fileForPath: [temp stringByStandardizingPath]];
                else
                    sfile = [self fileForPath: rel2abs(pPath,temp)];
                if(sfile)
                {
                    [searchFiles addObject: sfile];
                }
            }
            ENDFORALL;
         }
        IFSAVEDKEY(@"init")
        {
            //[openFiles addObjectsFromArray: [masterArray decodeObjectForKey: 1]];
            if([[masterArray decodeObjectForKey: @"init"] isEqualToString: @""]) initFile = nil;
            else
            {
                NSString *iiFile;
                iiFile = [masterArray decodeObjectForKey: @"init"];
                if(isAbsPath(iiFile))
                    initFile = [self fileForPath: [iiFile stringByStandardizingPath]];
                else
                    initFile = [self fileForPath: rel2abs(pPath, iiFile)];
                [initFileName setStringValue: [initFile name]];
                [searchFiles removeObject: initFile];
                [searchListView reloadData];
            }
        }
        
        IFSAVEDKEY(@"codeList")
            [codeList addEntriesFromDictionary: [masterArray decodeObjectForKey: @"codeList"]];
        IFSAVEDKEY(@"namedSearchList")
            [self setUseableNamedSearchList: [masterArray decodeObjectForKey: @"namedSearchList"]];
        IFSAVEDKEY(@"selectedNamedSearchList")
            if([[masterArray decodeObjectForKey: @"selectedNamedSearchList"] isEqualToString: @""] == NO)
                [searchListMenu selectItemWithTitle: [masterArray decodeObjectForKey: @"selectedNamedSearchList"]];
        IFSAVEDKEY(@"namedSearch")
            [namedSrch addEntriesFromDictionary: [masterArray decodeObjectForKey: @"namedSearch"]];
        IFSAVEDKEY(@"codeSets")
            [codeSets addEntriesFromDictionary: [masterArray decodeObjectForKey: @"codeSets"]];
        IFSAVEDKEY(@"fileSets")
            [self loadFileSets: [masterArray decodeObjectForKey: @"fileSets"]];
            //[self loadFileSetWithName: [masterArray decodeObjectForKey: 13]];
        IFSAVEDKEY(@"currFileSet")
            [fileSetCurrName setString: [masterArray decodeObjectForKey: @"currFileSet"]];
        IFSAVEDKEY(@"currCodeSet")
            [codeSetCurrName setString: [masterArray decodeObjectForKey: @"currCodeSet"]];
        IFSAVEDKEY(@"summReort")
            {
                [summReports addEntriesFromDictionary: [masterArray decodeObjectForKey: @"summReport"]];
                [self rebuildSummReportMenu];
            }
            [self setCodeListDirty];
            return YES;
            
}
@end
