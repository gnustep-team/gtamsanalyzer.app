/* MWExportWatcher */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface MWExportWatcher : NSObject
{
    IBOutlet id clipboardSwitch;
    IBOutlet id escapeSwitch;
    IBOutlet id fieldList;
    IBOutlet id formatMenu;
    IBOutlet id selectedSwitch;
    IBOutlet id tagSwitch;
    IBOutlet id theBoss;
    IBOutlet id theWindow;
    IBOutlet id thePane;
    IBOutlet id headerSwitch;
    IBOutlet id colDelim;
    IBOutlet id rowDelim;
    
    NSArray *columns;
    NSMutableArray *colPool, *colList;
    NSMutableArray *dndList;
    int dndNdx;

}
- (IBAction)exitExport:(id)sender;
- (IBAction)runExport:(id)sender;
- (void) doExport;
@end
