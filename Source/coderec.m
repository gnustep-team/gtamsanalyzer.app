//
//  coderec.mm
//  CocoaTams
//
//  Created by matthew on Mon Apr 15 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import "coderec.h"
#import "utils.h"


@implementation coderec
- (id) init
{
    [super init];
    theName = [[NSMutableString alloc] init];
    theCoder = [[NSMutableString alloc] init];
    record = [[NSMutableString alloc] init];
    comment = [[NSMutableString alloc] init];
    affiliatedInfo = [[NSMutableDictionary alloc] init];
    sectionCodes = [[NSMutableArray alloc] init];
    openRun = YES;
    lineStart = lineEnd = 0;
    hasInfo = NO;
    runType = DATARUN;
    return self;
}

-(void) dealloc {
    [theName release];
    [record release];
    [comment release];
    [affiliatedInfo release];
    [sectionCodes release];
    [super dealloc];
}
-(void) setAffiliatedInfo: (NSMutableDictionary *) ai
{
    [affiliatedInfo setDictionary: ai];
    hasInfo = YES;

}
-(NSMutableArray *) sectionCodes
{
    return sectionCodes;
}

-(void) addSectionCodes: (NSArray *) who
{
    [sectionCodes addObjectsFromArray: who];
}

-(void) addAffiliatedInfo: (NSMutableDictionary *) ai
{
    [affiliatedInfo addEntriesFromDictionary: ai];
    hasInfo = YES;

}

-(NSMutableDictionary *) affiliatedInfo {return affiliatedInfo;}

-(BOOL) hasInfo {return hasInfo;}
- (MWFile *) doc
{
    return doc;
}

- (void) setDoc: (MWFile *) who
{
    doc = who;
}


-(int) runType {return runType;}
-(void) setRunType: (int) type {runType = type;}
- (void) setName: (NSString *) myname 
{
    [theName setString: myname];
}

- (void) setCoder: (NSString *) myname
{
    [theCoder setString: myname];
}

- (void) setRecord: (NSString *) acode
{
    [record setString: acode];
}

- (void) setComment: (NSString *) acomment
{
   [comment setString: acomment];
}

- (void) setopen: (BOOL) isopen 
{
    openRun = isopen;
}
- (NSMutableString *) theCoder
{return theCoder;}
- (NSMutableString *) theName
{ return theName;}
- (NSMutableString *) record
{return record;}
- (NSMutableString *) comment
{return comment;}
- (BOOL) openRun
{return openRun;}
- (void) addChar: (unichar) c;
{
    [record ADDCHAR(c)];
}
-(void) addString: (NSString *)s
{
    [record appendString: s];
}
-(void) appendString: (NSString *)s
{
    [record appendString: s];
}
-(void) setBegin: (unsigned long) b {bloc = b;}
-(void) setEnd: (unsigned long) e {eloc = e;}
-(unsigned long) beginLoc {return bloc;}
-(unsigned long) endLoc {return eloc;}
- (void) setOpen: (BOOL) isopen{openRun = isopen;}
-(void) setLineStart:(unsigned) what {lineStart = what;}
-(void) setLineEnd:(unsigned) what {lineEnd = what;}
-(unsigned) lineStart {return lineStart;}
-(unsigned) lineEnd {return lineEnd;}

@end
