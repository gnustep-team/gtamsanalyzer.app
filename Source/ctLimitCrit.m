//
//  ctLimitCrit.m
//  CocoaTams
//
//  Created by matthew on Mon Apr 15 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import "ctLimitCrit.h"
#import "tams.h"


@implementation ctLimitCrit
-(NSMutableString *) name
{
    return name;
}

-(void) parseCoders: (NSMutableString *) someName
{
    NSMutableString *myName, *aName, *aBuff;
    
    MWCHAR ch;
    int i,l;
    l = [someName length];
    if(l == 0) return;
    aName = [[NSMutableString alloc] init];
    aBuff = [[NSMutableString alloc] init];
    myName = [[NSMutableString alloc] init];
    i = 0;
    while( i < l)
    {
	ch = [someName characterAtIndex: i];
	if(ch == '[') {i++; break;}
	if(ch == ' ') {i++; continue;}
	[myName ADDCHAR(ch)];
	i++;
    }
    if(i >= l - 1)
    {
	[self setName: myName];
        [myName release];
	[aBuff release];
	[aName release];
	return;
    }
    //copy the string into buffer
     while(((ch = [someName characterAtIndex: i]) != ']') && i < l)
   {
	[aBuff ADDCHAR(ch)];
	i++;
    }
    
    //break up the buffer
    i = 0;
    l = [aBuff length];
    for(i = 0; i < l; i++)
    {
	ch = [aBuff characterAtIndex: i];
	if(ch == ' ') continue;
	if(ch == ';' || i == l-1)
	{
	    if(i == l-1)
		[aName ADDCHAR(ch)];
	    trimNSS(aName);
	    if([aName length])
	    {
		if([aName characterAtIndex: 0] == '~')
		{
		    [self addCoder: [aName substringFromIndex: 1] modifier: @"~"];
		}
		else
		{
		    [self addCoder: aName modifier: @""];
		}
		[aName setString: @""];
	    }
	}
	else
	    [aName ADDCHAR(ch)];
    }
    [self setName: myName];
    
    [myName release];
    [aBuff release];
    [aName release];
}
    
    
-(id) initWithString: (NSString *) who
{
    MWCHAR c;
    [super init];
    if([who length] == 0) return self;
    c = [who characterAtIndex: 0];
    if(c == '>' || c == '*'|| c== '\'')
    {
	modif = c;
	name = [[NSMutableString alloc] initWithString: [who substringFromIndex: 1]];
	coders = [[NSMutableArray alloc] init];
	[self parseCoders: name];
	
    }
    else
    {
	modif = ' ';
	name = [[NSMutableString alloc] initWithString: who];
	coders = [[NSMutableArray alloc] init];
	[self parseCoders: name];
    }
    return self;
	
}
-(id) init
{
    [super init];
    name = [[NSMutableString alloc] init];
    coders = [[NSMutableArray alloc] init];
    
    modif = ' ';
    return self;
    
}
-(void) dealloc
{
    [name release];
    [coders release];
}

-(void) setName: (NSMutableString *) n
{
    [name setString: n];
}
-(NSMutableString *) getName;
{
    return name;
}

-(char) modif{ return modif;}
-(NSMutableArray *) getCoders
{
    return coders;
}
-(BOOL) areCoders
{
    if([coders count]) return YES;
    else return NO;
}
-(void) addCoder: (NSString *) who modifier: (NSString *) how
{
    NSMutableDictionary *rr;
    rr = [[NSMutableDictionary alloc] init];
    [rr setObject: [who copy] forKey: @"coderName"];
    [rr setObject: [how copy] forKey: @"modifier"];
    [coders addObject: rr];
    
}
@end
