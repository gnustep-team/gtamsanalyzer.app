//
//  bookmarkInfo.m
//  TamsAnalyzer
//
//  Created by matthew on Sun May 12 2002.
//  Copyright (c) 2002 Matthew Weinstein. All rights reserved.
//

#import "bookmarkInfo.h"


@implementation bookmarkInfo
-(id) init
{
    self = [super init];
    identifier = [[NSMutableString alloc] init];
    return self;
}
-(void) setLocation: (int) l
{
    location = l;
}

-(void) setIdentifier: (NSString *) s
{
    [identifier setString: s];
}

-(int) bmLocation
{
    return location;
}

-(NSString *) identifier
{
    return identifier;
}

@end
